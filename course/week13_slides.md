---
title: "Week XIII: The Twentieth Century" 
date: 2023-11-28
---

##

![Untitled, Rauschenberg](media/rauchenberg.jpg){height=80%}

##

![Unititled, Rauschenberg](media/rauchenberg2.jpg){height=80%}

##

![Flag, Jasper Johns](media/johns_flag.jpg){height=80%}

##

![WHAAM!, Lichtenstein](media/lichthenstein_whaam.jpg){height=80%}

##

![Drowning Girl, Lichtenstein](media/lichthenstein_drowning.jpg){height=80%}

##

![Ocean Park, Diebenkorn](media/diebenkorn.jpg){height=80%}

##

![Fallen Astronaut, Van Hoeydonck](media/fallen_astronaut.jpg){height=80%}

##

![Jackson Pollock](media/pollock.jpg){height=80%}

##

![Campbell's Tomato Soup, Warhol](media/campbells.jpg){height=80%}

##

![Fourth Sign, Tony Smith](media/fourth_sign.jpg){height=80%}


##

![Interior Scroll, Schneemann](media/interior_scroll.jpg){height=80%}

##

![McCarthyism](media/mccarthyism.jpg){height=80%}

##

![McCarthyism](media/mccarthyism2.jpg){height=80%}

##

![B. F. Skinner's "Teaching Machine"](media/skinner_machine.jpg){height=80%}

##

!["Breaking Family Ties"](media/rockwell_family_ties.jpg){height=80%}

##

![Sexist 1950s Advertisment](media/1950s_tide.jpg){height=80%}

##

![A "50s Family"](media/50s_7up.png){height=80%}

##

![Ranch House Advertisment](media/50s_ranch_house.png){height=80%}

##

!["Cookie Cutter" Suburbs](media/1950s_suburb.jpg){height=80%}

##

![*Howl* Cover](media/howl.jpg){height=80%}

##

![Segregated Schooling](media/segregated_schools.jpg){height=80%}

##

![Martin Luther King Jr.](media/mlkjr.jpg){height=80%}

##

![Malcom X](media/malcomx.jpg){height=80%}

##

![COINTELPRO letter to MLK Jr.](media/cointelpro_letter.jpg){height=80%}

##

![The Bronx, 1970s](media/bronx_storefronts.jpg){height=80%}

##

![The Bronx, 1970s](media/bronx_burning.jpg){height=80%}

##

![President Carter in South Bronx](media/bronx_carter.jpg){height=80%}


##

![The War on Drugs](media/crack.jpg){height=80%}

##

![Columbine Victims](media/columbine.jpg){height=80%}

##

![Batman Comic, 1960s](media/batman.jpg){height=80%}

##

![Spiderman Comic, 1960s](media/spiderman.jpg){height=80%}

##

![Moomin!](media/moomin.jpg){height=80%}

##

![The Little Prince](media/little_prince.jpg){height=80%}

##

![Eric Carle's first picture book](media/carle_brown_bear.jpg){height=80%}

##

![The Very Hungry Caterpillar](media/carle_catterpillar.jpg){height=80%}

##

![The Snowy Day, 1962](media/snowy_day.jpg){height=80%}

##

![Anthony Browne's Gorilla](media/anthony_browne.jpg){height=80%}

##

![Lazy Jane, Shel Silverstein](media/lazy_jane.jpg){height=80%}

##

![Acrobats, Shel Silverstein](media/acrobats.jpg){height=80%}

##

![Fred Rogers](media/fred_rogers.jpg){height=80%}

##

![Mr. Rogers cooling of with a neighbor](media/rogers_pool.jpg){height=80%}

##

![Kind of Blue](media/kind_of_blue.jpg){height=80%}

##

![Buddy Holly](media/buddy_holly.jpg){height=80%}

##

![Chuck Berry](media/chuck_berry.jpg){height=80%}

##

![Elvis Presely](media/elvis.jpg){height=80%}

##

![Guess who???](media/young_beatles.jpg){height=80%}

##

![The Beatles, 1967](media/beatles.jpg){height=80%}

##

![Woodstock '69](media/jimi_woodstock.jpg){height=80%}

##

![Hip Hop](media/tribe_called_quest.jpg){height=80%}

##

![Punk Rock](media/ramones.jpg){height=80%}
