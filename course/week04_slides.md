---
title: Aesthetic of the Middle Ages
subtitle: "Week IV: The Emergence of Universities"
date: 2023-09-26
---


##

![Carolingan Miniscule [(CC-BY-SA jpemery)](https://commons.wikimedia.org/wiki/File:Carolineminuscel.jpg)](media/carolingan_miniscule.jpg){height=80%}

##

![Freising Manuscript](media/freising.jpg){height=80%}

##

![Blackletter Specimen](media/blackletter.jpg){height=80%}

##

![The Effects of Good Government](media/good_government.jpg){height=80%}

##

![](media/medieval_classes.jpg){height=80%}

##

![](media/scholasticism.jpg){height=80%}

##

![St. Thomas Aquinas](media/st_thomas.jpg){height=80%}

##

![Hildegard von Bingen](media/hildegard.jpg){height=80%}

##

![Trepanation](media/trepanation.jpg){height=80%}

##

![The Crusades](media/crusades.jpg){height=80%}

##

![Al-Quarawiyin [CC-BY-SA Ahmed.magdy ](https://commons.wikimedia.org/wiki/File:Bou_Inania_Madrasa,Fes.jpg)](media/al-qarawiyin.jpg){height=80%}


##

![University of Bologna [CC-BY-SA Conte di Cavour](https://commons.wikimedia.org/wiki/File:Archiginnasio-bologna02.png)](media/bologna_uni.png){height=80%}

##

![Oxford University [CC-BY-SA Diliff](https://commons.wikimedia.org/wiki/File:Divinity_School_Interior_2,_Bodleian_Library,_Oxford,_UK_-_Diliff.jpg)](media/oxford.jpg){height=80%}

##

![Doctors of University of Paris (15th Century)](media/paris_uni.jpg){height=80%}

##

![](media/courtly_mirror.jpg){height=80%}

##

![](media/court_of_love.jpg){height=80%}

##

![Abelard and Heloise](media/abelard_heloise.jpg){height=80%}

##

![Canterbury Tales](media/canterbury_tales.jpg)

##

![](media/canterbury_woodblock.jpg){height=80%}

##

![Dante](media/dante.jpg){height=80%}

##

![Troubador](media/troubador.jpg){height=80%}

##

![Monreale Mosaic [(CC-BY-SA Berthold Werner)](https://commons.wikimedia.org/wiki/File:Monreale_BW_2012-10-09_09-52-40.jpg)](media/monreale_mosaic.jpg){height=80%}

##

![Apse of Monreale Cathedral](media/monreale_apse.jpg){height=80%}

##

![Gothic Sculpture, Chartres [(CC-BY-SA TTaylor)](https://commons.wikimedia.org/wiki/File:Chartres_cathedral_023_martyrs_S_TTaylor.JPG)](media/chartres_sculpture.jpg){height=80%}

##

![Sainte-Chapelle [(CC-BY-SA Didier B)](https://commons.wikimedia.org/wiki/File:Sainte_Chapelle_-_Upper_level_1.jpg)](media/chapelle_upper.jpg){height=80%}

##

![Sainte-Chapelle, crypt[(CC-BY-SA Ben Lieu Song)](https://commons.wikimedia.org/wiki/File:Ste_Chapelle_Basse_s.jpg)](media/chapelle_basement.jpg){height=80%}

##

![Sainte-Chapelle, gables [(CC-BY-SA Zairon)](https://commons.wikimedia.org/wiki/File:Paris_Sainte-Chapelle_Giebel.jpg)](media/chapelle_gables.jpg){height=80%}

##

![Sainte-Chapelle, detail](media/chapelle_creation.jpg){height=80%}

##

![Sainte-Chapelle, detail[(CC-BY-SA Thomon)](https://commons.wikimedia.org/wiki/File:Est_Sainte-Chapelle_Passion_du_Christ_Abside.jpg)](media/chapelle_passion.jpg){height=80%}

##

![Moasic & Arches [(CC-BY-SA Megginede)](https://commons.wikimedia.org/wiki/File:Leon_(San_Isidoro,_pante%C3%B3n).jpg)](media/spanish_crypt.jpg){height=80%}

##

![Dipthych](media/richard_diptych.jpg){height=80%}

##

![Madonna and Child](media/madonna_and_child.jpg){height=80%}

##

![Historicization](media/historicized_r.jpg){height=80%}

##

![La Chanson de Roland](media/roland.jpg){height=80%}

##

![Death of Wat Tyler](media/wat_tyler_death.jpg){height=80%}

##

![Murder of Thomas Becket](media/thomas_becket.jpg){height=80%}

##

![Execution of Jacquerie Participant](media/jacquerie.jpg){height=80%}

##

![Agricultural Calendar](media/medieval_calendar.jpg){height=80%}

##

![Reconstruction of the Temple of Jerusalem](media/jerusalem_reconstruction.jpg){height=80%}

##

![Reeve & Serfs](media/reeve_serfs.jpg){height=80%}

##

![Manorialism](media/manorialism.jpg){height=80%}

##

![Open Field System](media/open_field.jpg){height=80%}

## 

![Arabic Numerals (10th C.)](media/arabic_numerals.jpg){height=80%}

##

![Liber Abbaci](media/liber_abbaci.jpg){height=80%}

##

![Danse Macabre](media/dance_death.jpg){height=80%}

##

![Black Death](media/black_death.jpg){height=80%}

##

![Persecution of Jews after the Plague](media/black_death_jews.jpg){height=80%}
