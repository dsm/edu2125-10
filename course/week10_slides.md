---
title: Progress, Nature, Romance
subtitle: "Week X: The Nineteenth Century"
date: 2023-11-07
---

# Recap

## Recap

- Freedom vs. Inequality: self-conscious response to indigenous critique
  - Hierarchy of Cultural Development
  - Society as a compromise: property and stability in exchange for inequality and limited freedoms
  - The Noble Savage -> The Stupid or Simple Savage
  - Ambivalence about education and qualitiy of life of the poor (except moral conformity); emphasis on individual success and failure
- Nature vs Nurture
  - "Child-centric" education; individual capacities and experiences
  - Humans as inherently good; "negative education" as damage control
- Basedow's philanthropy
  - represents gradual movement to appreciate goodness and innocence of children
  - brings attention to the need for adults to be re-educated about handling children
  - trend of education administered by and for the "concerned public"


# Post-Revolutionary Europe

##

![Napoleon Bonaparte](media/napoleon.jpg){height=80%}

##

![Georg Friedrick Wilhelm Hegel](media/hegel_portrait.jpg){height=80%}

##

![Communist Manifesto](media/communist_manifesto.jpg){height=80%}

##

![Queen Victoria, 1844](media/queen_victoria.jpg){height=80%}

##

![British Empire in 1886](media/british_empire_1886.jpg){height=80%}

##

![Europe in 1815 [(CC-BY-SA Alexander Altenhof)](https://commons.wikimedia.org/wiki/File:Europe_1815_map_en.png)](media/europe_1815.jpg){height=80%}

##

::: notes

- mechanics institutes
  - founded by industrialists on premise that more knowledgeable and skilled employees would benefit industry
  - had libraries
  - for adult working class
- cost 15 shillings (about 45 pounds now, or a week worth of wages for a skilled tradesman)
  - gave you access to lectures and library for a year
  - had to shut down registration because of oversubscription
- only men allowed for decades
  - if there is a benefit to educating industrial labor force, women were at a major disadvantage
  
:::


![Edinburgh School of Arts [(CC-BY-SA Kim Traynor)](https://commons.wikimedia.org/wiki/File:Chambers_Street,_Edinburgh_-_geograph.org.uk_-_1419940.jpg)](media/edinburgh_arts.jpg){height=80%}

##

![New England Academy Movement](media/amherst.jpg){height=80%}

::: notes

- alternative to grammar schools and private tutoring
  - relatively modest fees
  - idea that a free republic needed educated citizens
- separate sexes, and co-ed
  - but actual education differed

:::

##

![Common School Movement](media/common_school.png){height=80%}

::: notes

- standardize education
  - trend of putting power to determine education in hands of central government
  - moral citizens, christian
- normal schools to educate teachers that use better methods
  - teachers would be women; people were uneasy putting moral education of their kids in hands of randos

:::

##

![Lyceum Movement](media/lyceum_movement.jpg){height=80%}

::: notes

- public adult education, free, somewhat anti-establishment
- self-determination
- associated with social libraries
- not just to acquire info, but to experience different worlds, cultures, ideas in community context

:::

##

![Temperance Movement](media/drunkards_progress.jpg){height=80%}

::: notes

- connection to informal and formal education
  - creating a new moral society
  - not just public health, but individual self-control and ability to contribute to society

:::


# Romanticism

##

![Wanderer above the Sea of Fog, Caspar David Friedrich](media/friedrich_wanderer.jpg){height=80%}

##

![The Morning, by Philipp Otto Runge](media/runge_morning.jpg){height=80%}

##

![The Fighting Temeraire, J. M. W. Turner](media/turner_temeraire.jpg){height=80%}

##

![Fisherman at Sea, Turner](media/turner_fisherman.jpg){height=80%}

##

![Hylas and the Nymphs, Waterhouse](media/waterhouse_nymphs.jpg){height=80%}

##

![Franz Liszt Fantasizing at the Piano, Josef Danhauser](media/danhauser_liszt_piano.jpg){height=80%}

##

![Neo-Classical vs Romantic Style](media/neo-classical_vs_romantic.jpg){height=80%}

##

![Ozymandias by Percy Bysshe Shelley](media/oxymandias.jpg){height=80%}

##

![Dandy](media/dandy.png){height=80%}

##

![Oscar Wilde](media/oscar_wilde.jpg){height=80%}

# Manifest Destiny

##

![American Progress, or Spirit of the Frontier](media/american_progress.jpg){height=80%}

##

![Bill advertizing passage to California from New York](media/gold_rush_ad.jpg){height=80%}

##

![San Francisco Harbor in 1851](media/sf_harbor.jpg){height=80%}

##

![Gold Rush Prospecting Locations [(CC-BY-SA Hans van der Maarel)](https://commons.wikimedia.org/wiki/File:CaliforniaGoldRush.png)](media/california_gold_map.jpg){height=80%}

##

![Cultural Diversity of Indigenous Californians [(CC-BY-SA Concerto)](https://commons.wikimedia.org/wiki/File:California_tribes_%26_languages_at_contact.png)](media/cali_tribes.png){height=80%}


##

![The Oregon Trail [(CC-BY)](https://commons.wikimedia.org/wiki/File:Wpdms_nasa_topo_oregon_trail.jpg)](media/oregon_trail.jpg){height=80%}

##

![Map of Western US in 1846](media/oregon_cali_texas_map.jpg){height=80%}

##

![The Trail of Tears](media/trail_of_tears.jpg){height=80%}

##

![Map of Indian Territory](media/indian_territory.jpg){height=80%}

# Civil Rights & Transcendentalism

##

![Woman in the 19th Century by Margaret Fuller](media/woman_in_19th_century.jpg){height=80%}

##

![The Great Lawsuit by Margaret Fuller in The Dial](media/great_lawsuit.jpg){height=80%}

##

![Margaret Fuller](media/margaret_fuller.jpg){height=80%}

##

![Woman's Rights Convention [(CC-BY-SA)](https://commons.wikimedia.org/wiki/File:Woman%27s_Rights_Convention.jpg)](media/womens_rights_convention.jpg){height=80%}

##

![Sojourner Truth](media/sojourner_truth.jpg){height=80%}

##

![Frederick Douglas](media/frederick_douglas.jpg){height=80%}

##

![Ralph Waldo Emerson](media/emerson.jpg){height=80%}

## From "New England Reformers"

> I notice too, that the ground on which eminent public servants urge the claims of popular education is fear: ‘This country is filling up with thousands and millions of voters, and you must educate them to keep them from our throats.’ We do not believe that any education, any system of philosophy, any influence of genius, will ever give depth of insight to a superficial mind. Having settled ourselves into this infidelity, our skill is expended to procure alleviations, diversion, opiates. We adorn the victim with manual skill, his tongue with languages, his body with inoffensive and comely manners. So have we cunningly hid the tragedy of limitation and inner death we cannot avert. Is it strange that society should be devoured by a secret melancholy, which breaks through all its smiles, and all its gayety and games?

##

![Henry David Thoreau](media/thoureau.jpg){height=80%}

## Thoreau on Education

- Education makes a straight ditch of a free meandering brook.

- We saw one school-house in our walk, and listened to the sounds which issued from it; but it appeared like a place where the process, not of enlightening, but of obfuscating the mind was going on, and the pupils received only so much light as could penetrate the shadow of the Catholic church.

- We boast of our system of education, but why stop at schoolmasters and schoolhouses? **We are all schoolmasters, and our schoolhouse is the universe**. To attend chiefly to the desk or schoolhouse while we neglect the scenery in which it is placed is absurd. If we do not look out we shall find our schoolhouse standing in a cow-yard at last.
