---
title: The Aesthetics of The Roman Empire and the Dark Ages
subtitle: "Week III: The Roman Empire & The Dark Ages"
date: 2023-09-19
---

# The Roman Empire

## General Characteristics

* wide variety of art forms were produced
  * copies and imitations of looted foreign works
  * adopted materials and styles of diverse regions in empire
* art became a way to beautify everyday life
  * beautiful mundane objects became more widely coveted and available
  * became more affordable
  * more personalized
* more generalistic subject matter
  * became a way to record history as opposed to mythical and folkloric depictions
  * mundane, ordinary life & landscapes, etc.
* realism was the ideal
  * accurate portraits of the deceased

# Roman Architecture & Lifestyle

##

![Pantheon Oculus [(CC-BY-SA Mariordo)](https://commons.wikimedia.org/wiki/File:Oculus_Pantheon_Rome_04_2016_6435.jpg)](media/pantheon_oculus.jpg){height=80%}

##

![Colosseum [(CC-BY-SA Jeal-Pol Grandmont)](https://commons.wikimedia.org/wiki/File:0_Colosseum_-_Rome_111001_(2).JPG)](media/colosseum.jpg){height=80%}

##

![Roman Bath [(CC-BY-SA Diliff)](https://commons.wikimedia.org/wiki/File:Roman_Baths_in_Bath_Spa,_England_-_July_2006.jpg)](media/roman_bath.jpg){height=80%}

##

![Pistrinum [CC-BY-SA Carole Raddato](https://commons.wikimedia.org/wiki/File:The_Pistrinum_(bakery)_on_Vicolo_Storto_(Reg_VII,_Ins_2,_22)_belonging_to_N._Popidius_Priscus,_Pompeii_(14856682197).jpg)](media/pistrinum.jpg){height=80%}

## 

![Grande Taberna [CC-BY-SA Aldo Adretti](https://commons.wikimedia.org/wiki/File:GrandeTaberna.JPG)](media/grande_taberna.jpg){height=80%}

## 

![Thermopolium at Pompeii [CC-BY Daniele Florio](https://commons.wikimedia.org/wiki/File:Ancient_Bar,_Pompeii.jpg)](media/pompeii_thermopolium.jpg){height=80%}

##

![Popina at Pompeii [CC-BY ell brown](https://commons.wikimedia.org/wiki/File:La_popina_du_bordel_de_Pomp%C3%A9i.jpg)](media/popina_pompeii.jpg){height=80%}

##

![Inusla in Rome [CC-BY-SA Chabe01](https://commons.wikimedia.org/wiki/File:Insula_Ara_Coeli_-_Rome_(IT62)_-_2021-08-27_-_2.jpg)](media/insula_rome.jpg){height=80%}

##

![Insula-lined street [CC-BY lessi](https://commons.wikimedia.org/wiki/File:Ostia_Antica-strada01-modified.jpg)](media/insula_street.jpg){height=80%}

##

![Roman Domus Floorplan [CC-BY-SA PureCore](https://commons.wikimedia.org/wiki/File:Domus_romana_Vector002.svg)](media/domus_layout.png){height=80%}

##

![Atrium inside a Domus](media/atrium.jpg){height=80%}

## 

![Reconstructed Peristyle [CC-BY-SA saiko](https://commons.wikimedia.org/wiki/File:Ricostruzione_del_giardino_della_casa_dei_vetii_di_pompei_(mostra_al_giardino_di_boboli,_2007)_01.JPG)](media/peristyle.jpg){height=80%}

##

![Tablinum of Pompeiian Domus [CC-BY-SA Carole Raddato](https://commons.wikimedia.org/wiki/File:The_tablinum_of_the_House_of_Menander_(Regio_I),_Pompeii_(14978936569).jpg)](media/tablinum.jpg){height=80%}

##

![Lararium [CC-BY-SA Waterborough](https://commons.wikimedia.org/wiki/File:Casa_dei_Vettii_-_Larario.jpg)](media/lararium.jpg){height=80%}

##

![Bronze Lar [CC-BY-SA Luis Garcia](https://commons.wikimedia.org/wiki/File:Lar_romano_de_bronce_(M.A.N._Inv.2943)_01.jpg)](media/lar.jpg){height=80%}

##

![Triclinium Restoration](media/triclinium.jpg){height=80%}

##

![Roman Graffito [CC-BY-SA The Commonist](https://commons.wikimedia.org/wiki/File:Inscription_on_Wall_Plaster_-_Roman_Museum_-_Augusta_Raurica_-_August_2013.JPG)](media/graffito.jpg){height=80%}

##

![Roman Pornography](media/roman_porn.jpg){height=80%}

##

![Genitalia-shaped Votive Offerrings](media/genital_votives.jpg){height=80%}

##

![Depiction of Pederasty](media/roman_pederasty.jpg){height=80%}

##

![Toga Praetexta](media/toga_praetexta.png){height=80%}

##

![Bulla depicting Icarus](media/bulla.jpg){height=80%}

##

![Lunula](media/lunula.jpg){height=80%}

# Roman Sculpture

## 

::: notes

* followed Greeks in their idealization of perfection, but preferred realistic representations

* preservation of Greek originals

* copies of Roman and Greek originals, of all sizes

* favored bronze and marble, but mostly marble sculpture has survived

* highly detailed reliefs on friezes, cameos, etc.

:::

![Roman Portrait Bust ([CC-BY-SA-NC Mark Cartwright](https://www.worldhistory.org/image/2059/roman-portrait-bust/))](media/roman_portrait_bust.jpg){height=80%}

##

![Realistic Bust](media/warts_bust.jpg){height=80%}

##

![Bust of Old Man [(CC-BY-SA shakko)](https://commons.wikimedia.org/wiki/File:Old_man_vatican_pushkin01.jpg)](media/old_man_bust.jpg){height=80%}

##

![Frieze on Arch of Constantine [(CC-BY-SA Radomil](https://commons.wikimedia.org/wiki/File:Luk_Konstantyna_6DSCF0032.JPG)](media/arch_of_constantine.jpg){height=80%}

##

![Relief depicting animal sacrifice](media/sacrifice_relief.jpg){height=80%}

##

![Detail of Colonna Traiana [(CC-BY-SA Matthias Kabel)](https://commons.wikimedia.org/wiki/File:26_colonna_traiana_da_estt_05.jpg)](media/colonna_traiana.jpg){height=80%}

##

![The Great Cameo of France [(CC-BY-SA Janmad)](https://commons.wikimedia.org/wiki/File:Great_Cameo_of_France_CdM_Paris_Bab264_white_background.jpg)](media/cameo_of_france.jpg){height=80%}

##

![Cameo of Augustus](media/cameo_augustus.jpg){height=80%}

##

![Terracotta Figurine [(CC-BY-SA Alphanidon)](https://commons.wikimedia.org/wiki/File:Terracotta_Aeneas_MAN_Naples_110338.jpg)](media/aeneas_figurine.jpg){height=80%}

##

![Coin depicting Hadrian [(CC-BY-SA CNG)](https://commons.wikimedia.org/wiki/File:HADRIANUS_RIC_II_938-789065.jpg)](media/hadrian_coin.jpg){height=80%}

# Roman Crafts

## 

::: notes

* tremendous variety
  * jewelry, weaponry, utensils & vessels, furniture, clothing etc.
  * influenced by and produced using materials and objects from across the empire
* representative of "personalization" of art in the empire

:::

![Miscellaneous Roman Pottery [(CC-BY-SA AgTigress)](https://commons.wikimedia.org/wiki/File:Roman_pottery_from_Britain.jpg)](media/msc_roman_pottery.jpg){height=80%}

## 

![Roman Perfume Bottle ([CC-BY-SA-NC Mark Cartwright](https://www.worldhistory.org/image/4595/roman-perfume-bottle/))](media/roman_perfume.jpg){height=80%}

##

![The Portland Vase [(CC-BY Jastrow)](https://commons.wikimedia.org/wiki/File:Portland_Vase_BM_Gem4036_n5.jpg)](media/portland_vase.jpg){height=80%}

##

![Blown Vial](media/roman_vial.jpg){height=80%}

##

![The Munich Cup](media/munich_cup.jpg){height=80%}

##

![Luxury Pyxis](media/roman_pyxis.jpg){height=80%}

##

![Guilded Glass Medallion](media/guilded_glass.jpg){height=80%}

# Roman Painting


## 

::: notes

* encaustic painting method: hot wax and pigment

Frescoes:
* typical subject matter included:
  * landscapes
  * plant and wildlife
  * garden scenes
  * townscapes & architecture
  * mythic or folkloric scenes
* compositions usually included earth tones and muted accent colors

Mummy Portraits:

* portraits painted on wooden boards, placed over faces of mummies

* painted to the likeness of the deceased

:::

![Fresco at Livia's Villa [CC-BY-SA-NC Mark Cartwright](https://www.worldhistory.org/image/1161/fresco-livias-villa-rome/)](media/livia_fresco_bird.jpg){height=80%}

## 

![Zephyrus and Chloris [CC-BY Stefano Bolognini](https://commons.wikimedia.org/wiki/File:Zeffiro-e-clori---pompeii.jpg)](media/zeffiro_fresco.jpg){height=80%}

##

![Impressionistic Fresco](media/boscotrecase.jpg){height=80%}

##

![Livia's Villa, Garden Scene [CC-BY Wellcome Gallery](https://commons.wikimedia.org/wiki/File:A_border_of_trees,_palms_and_herbaceous_plants_from_a_Roman_Wellcome_V0044515.jpg)](media/livia_fresco_garden.jpg){height=80%}

##

::: columns

:::: column

![](media/mummy_portrait1.jpg){height=80%}

::::

:::: column

![](media/mummy_portrait2.jpg){height=80%}

::::

:::

##

::: columns

:::: column

![](media/mummy_portrait3.jpg){height=80%}

::::

:::: column

![](media/mummy_portrait4.jpg){height=80%}

::::

:::

# Roman Mosaics

##

![The Alexander Mosaic](media/alexander_mosaic.jpg){height=80%}

##

![Life on the Nile](media/life_on_the_nile.jpg){height=80%}

##

![Life on the Nile (detail)](media/life_on_the_nile_detail.jpg){height=80%}

##

![Gladiator Mosaic](media/gladiator_mosaic.jpg){height=80%}

# Dark Ages

## Dark Ages


* religious themes
  * most art was produced or commissioned by clergy
	* ornamentation of sacred or religious objects
* no interest in realism
  * realistic paintaings and sculptures were considered idolartry
  * characteristic lack of lighting and perspective


# Early Middle Ages Architecture

##

![Walls of Constantinople](media/constantinople_walls.jpg){height=80%}

##

![Basilica of San Vitale [(CC-BY-SA Commonists)](https://commons.wikimedia.org/wiki/File:Basilica_of_San_Vitale.jpg)](media/san_vitale.jpg){height=80%}

##

![Basilica of San Vitale (inside) [(CC-BY-SA Isatz)](https://commons.wikimedia.org/wiki/File:2017_0423_Ravenna_(132).jpg)](media/ravenna.jpg){height=80%}

##

![Hagia Sophia [(CC-BY-SA Arild Vågen)](https://commons.wikimedia.org/wiki/File:Hagia_Sophia_Mars_2013.jpg)](media/hagia_sophia.jpg){height=80%}

# Byzantine Paintings and Mosaics

##

![Altarpiece](media/byzantine_altarpiece.jpg){height=80%}

##

![Elaborate Altarpiece](media/byzantine_altarpiece2.jpg){height=80%}

##

![Fresco](media/byzantine_fresco.jpg){height=80%}

##

![Mosaic of Justinian [(CC-BY-SA Carole Raddato)](https://commons.wikimedia.org/wiki/File:The_mosaic_of_Emperor_Justinian_and_his_retinue.jpg)](media/justinian_mosaic.jpg){height=80%}

## 

![Ceiling Mosaic](media/ceiling_mosaic.jpg){height=80%}

# Metalwork

##

![Sutton Hoo Shoulder Clasp [(CC-BY-SA Rob Roy)](https://commons.wikimedia.org/wiki/File:Sutton.Hoo.ShoulderClasp2.RobRoy.jpg)](media/shoulder_clasp.jpg){height=80%}

##

![Fancy Fibulae [(CC-BY-SA James Steakley)](https://commons.wikimedia.org/wiki/File:Paar_Prunkfibeln.jpg)](media/fibulae.jpg){height=80%}

##

![Stowe Missal Cumdach [(CC-BY Sailko)](https://commons.wikimedia.org/wiki/File:Contenitore_del_messale_con_iscrizione_al_%27re%27_d%27irlanda_donnchad,_1030_ca.,_dal_monastero_di_lorrha,_co._di_tipperary,_02.jpg)](media/stowe_missal_cumdach.jpg){height=80%}

##

![Stowe Missal Cumdach, side view [(CC-BY Sailko)](https://commons.wikimedia.org/wiki/File:Contenitore_del_messale_con_iscrizione_al_%27re%27_d%27irlanda_donnchad,_1030_ca.,_dal_monastero_di_lorrha,_co._di_tipperary,_04.jpg)](media/stowe_missal_cumdach_side.jpg){height=80%}

##

![Codex Aureus Sankt Emmeram](media/codex_aureus.jpg){height=80%}

##

![Monymusk Reliquary](media/monymusk_reliquary.jpg){height=80%}

# Monuments

##

![Muiredach High Cross [(CC-BY-SA Kit36a)](https://commons.wikimedia.org/wiki/File:Muiredach_keresztje.jpg)](media/muiredach.jpg){height=80%}

##

![Hilton of Cadboll Stone](media/hilton_of_cadboll.jpg){height=80%}

# Iluminations

##

![](media/illumination1.jpg){height=80%}

##

![](media/illumination2.jpg){height=80%}

##

![](media/illumination3.jpg){height=80%}

##

![](media/illumination4.jpg){height=80%}

##

![](media/illumination5.jpg){height=80%}
