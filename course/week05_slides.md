---
title: The Renaissance Humanistic Ideal
subtitle: "Week V: The Renaissance & Humanistic Education"
date: 2023-10-03
---

# Recap

##

- emergence of universities coincides with social and economic shift in European societies
  - money economy, larger middle class, specialized trades (guilds)
  - increased number of civic institutions run and operated by laity
- consequence of scholasticism, increased interest in philosophy, greater production of books
  - specialized "professional" studies
  - "research" and inquiry vs. textual transcription and transmission

##

- originated in "guilds" of scholars, studia generalia
  - high numbers of foreigners drawn to the studia
  - original model represented a "co-op": commons governance
  - higher education created need for social reorganization and adaptation of church policy
  - the "university" developed to address this need
    - to collectively bargain for relative "academic freedom" and "self-determination"
- university estabished as a semi-autonomous entity and higher education as a socical institution

##

- private vs. public university
  - distinction did not really exist then as it does now
  - attending university cost money; colleges emerged partly as a way to accommodate poor scholars
- liberal arts were the universal core of education, but education was not universal or comprehensive
  - education was highly opportunistic; many bachelors did not become masters
- higher education was not associated with occupations as it is today
- university masters were not necessarily universally respected, nor were their degrees acknowledged as conferring complete authority

# Moveable Metal Type

##

![직지심체, 1377](media/직지심체.jpg){height=80%}

::: notes

- from 고려
- oldest existing book printed with moveable metal type
- compendium of teachings of zen masters

:::

##

![직지심체, galley](media/직지심체_type.jpg){height=80%}

##

![Diamond Sutra Frontispiece, woodblock (c. 868)](media/diamond_sutra.jpg){height=80%}

::: notes

- from tang dynasty
- oldest dated printed book in existence

- why was lating printing so successful compared to chinese and korean, even with moveable type?
  - over 11,000 possible combinations of hangul characters excluding obsolete ones
  - around time moveable type was invented, there wer at least 50,000 hanja in use. that number is now at least doubled if we include variant forms etc.

- even with moveable type where you can create your sorts from a template, it is still unwieldy and expensive in asian languages
- variety in typfaces in latin

:::

##

![Gutenberg Press [(CC-BY vlasta)](https://commons.wikimedia.org/wiki/File:PrintMus_038.jpg)](media/gutenberg_press.jpg){height=80%}

##

![Moveable Type & Composition Stick [(CC-BY-SA Willi Heidelbach)](https://commons.wikimedia.org/wiki/File:Metal_movable_type.jpg)](media/type_and_stick.jpg){height=80%}

##

![Type Case [(CC-BY Maggie McCain)](https://commons.wikimedia.org/wiki/File:Upper_case_and_lower_case_types.jpg)](media/type_case.jpg){height=80%}

##

![Typesetting Tools [(CC-BY Wilhei)](https://commons.wikimedia.org/wiki/File:Handsatz_Zubehoer.jpg)](media/typesetting_tools.jpg){height=80%}

##

![Stereotype [(CC-BY Edinburgh City of Print)](https://commons.wikimedia.org/wiki/File:New_Testament_in_chase.jpg)](media/stereotype.jpg){height=80%}

##

!["Antique" Roman Type](media/trajan_type.png){height=80%}

::: notes

- based on trajan's column, all caps

:::

##

![Humanistic Miniscule](media/humanistic_miniscule.jpg){height=80%}

::: notes

- attempt to create script that resembles original latin
- fusion of antique romaan capitols and carolingan miniscule
- creators did not know that car. minsc. was not original romans script! additionally, they thought blackletter was "gothic" script used by the goths who overtook the roman empire!!! 
- point is that even though we see history as a consistent, coherent story, it is full of disjunctions. their perception of their place in the world is much different than how we perceive their situation
- good to be mindful of this as we approach modern history

:::

##

![Jensen "roman" type](media/jensen_roman.png){height=80%}

::: notes

- original "roman" typeface designed to NOT imitate handwriting.
:::

##

![Gutenberg Bible](media/gutenberg_bible.jpg){height=80%}

::: notes

- printed in blackletter
- most printed book once press was invented
- kind of history repeating itself: just like monks making illuminated copies of sacred texts in the dark ages

:::

##

![World map of Pietro Coppo, Venice, 1520](media/world_map_coppo.jpg){height=80%}

# Renaissance Paintings

##

![Garden of Earthly Delights by Heironymous Bosch](media/earthly_delights.jpg){height=80%}

##

![Raphael Room in Apostolic Palace](media/raphael_stanza.jpg){height=80%}

::: notes

- in vatican

:::

##

![School of Athens, by Raphael](media/school_of_athens.jpg){height=80%}

##

![Birth of Venus, by Botticelli](media/birth_of_venus.jpg){height=80%}


::: notes

- hora of spring on right suggests the gesture of a babtism, interesting fusion of mythology and religion. expresses the consciousness of humanism and its desire to reinvent european culture and identity

:::

##

![Primavera, by Botticelli](media/botticelli_primavera.jpg){height=80%}

::: notes

- mythological allegory;
- uncertain meaning, but seems to express the "burgeoning fertility of the world"
- reflects the optimism of the renaissance and desire to grow away from the "dark ages"

:::

##

![Sacred and Profane Love by Titian](media/sacred_profane_love.jpg){height=80%}

::: notes

- notable that many renaissance painters did not devise the concepts for their paintings themselves; they were commissioned, and sometimes the idea could come from some master of the classics
- reveals a lot about the dynamics of the intellectual world then

:::

# Renaissance Polymathy

## 

![Portrait of Michelangelo by Daniele da Volterra, c. 1545](media/michelangelo.jpg){height=80%}

## Poem for Cavalieri by Michelangelo

```
I feel as lit by fire a cold countenance
That burns me from afar and keeps itself ice-chill;
A strength I feel two shapely arms to fill
Which without motion moves every balance.
```

##

![Sistine Chapel [(CC-BY-SA Jean-Christophe Benoist)](https://commons.wikimedia.org/wiki/File:Vatican-ChapelleSixtine-Plafond.jpg)](media/sistine_chapel.jpg){height=80%}

##

![Sistine Chapel, whole work [(CC-BY-SA Qypchak)](https://commons.wikimedia.org/wiki/File:CAPPELLA_SISTINA_Ceiling.jpg)](media/sistine_chapel_whole.jpg){height=80%}

##

![Last Judgement](media/last_judgement.jpg){height=80%}


##

![David [(CC-BY-SA Marcus Obal)](https://commons.wikimedia.org/wiki/File:Michelangelo%27s_David.JPG)](media/david.jpg){height=80%}

##

::: columns

:::: column

![David (detail) [(CC-BY Jörg Bittner Unna)](https://commons.wikimedia.org/wiki/File:%27David%27_by_Michelangelo_JBU10.JPG)](media/david_detail1.jpg){height=80%}

::::

:::: column

![David (detail) [(CC-BY Jörg Bittner Unna)](https://commons.wikimedia.org/wiki/File:%27David%27_by_Michelangelo_JBU08.JPG)](media/david_detail2.jpg){height=80%}

::::

:::

##

![La Pieta [(CC-BY-SA Stanislav Traykov)](https://commons.wikimedia.org/wiki/File:Michelangelo%27s_Pieta_5450_cut_out_black.jpg)](media/pieta.jpg){height=80%}


##

![Laurentian Library [(CC-BY-SA Stuart Cahill)](https://commons.wikimedia.org/wiki/File:LaurentianLibFromStairway.jpg)](media/laurentian_library.jpg){height=80%}


##

![Leonardo Da Vinci, self-portrait](media/davinci.jpg){height=80%}

##

![Da Vinci's "Helicopter"](media/davinci_helicopter.jpg){height=80%}

##

![Da Vinci's Flying Machine](media/davinci_flying_machine.jpg){height=80%}

##

![Da Vinci's Anatomy Notes](media/davinci_brain.jpg){height=80%}

##

![Da Vinci's Anatomy Notes](media/davinci_arms.jpg){height=80%}

##

![Da Vinci's Anatomy Notes](media/davinci_fetus.jpg){height=80%}

##

![Vitruvian Man](media/vitruvian_man.jpg){height=80%}

##

![Da Vinci's Map of Imola](media/davinci_imola.jpg){height=80%}

##

![Unfinished Painting of St. Jerome](media/davinci_stJerome.jpg){height=80%}
