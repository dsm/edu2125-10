---
title: "Week XIV: The Twenty-first Century" 
date: 2023-12-05
---


##

![IBM Simon charging](media/simon_phone.jpg){height=80%}

##

![IBM Simon and iPhone 5](media/simon_iphone.jpg){height=80%}

##

![Vaporwave](media/vaporwave.jpg){height=80%}

##

![Corporate Memphis, Globohomo, Big Tech style, flat art, etc.](media/corporate_memphis.jpg){height=80%}

##

![March for Our Lives, D.C. 2018](media/mfol.jpg){height=80%}

##

![G8 Convention sabotage attempt, Torino, 2001](media/g8_torino.jpg){height=80%}

##

![New Orleans after Hurricane Katrina, 2005](media/katrina.jpg){height=80%}

##

![Anthropocene (Holocene) Extinction Event](media/gorilla.jpg){height=80%}

##

![Solarpunk](media/solarpunk.jpg){height=80%}

##

![September 11th, 2001](media/9-11.jpg){height=80%}

##

![Refugees in Slovenia, 2015](media/slovenia_immigrants.jpg){height=80%}

##

![ICE Familiy Separation Protest, USA](media/families_belong_together.jpg){height=80%}

##

![Occupy Wall Street & The People's Library, 2011](media/occupy.jpg){height=80%}

##

![Myspace](media/myspace.jpg){height=80%}

##

![Example Myspace Profile](media/myspace_profile.jpg){height=80%}

##

![Vlogging!](media/youtube.jpg){height=80%}

##

![Digital Rights](media/eff.jpg){height=80%}

##

![Wikipedia](media/wikipedia.org){height=80%}

##

![Creative Commons, est. 2001](media/creative_commons.jpg){height=80%}

##

![Open Access](media/open_access.jpg){height=80%}

##

![WikiLeaks, 2006](media/wikileaks.org){height=80%}

## 

![Julian Assange](media/assange.jpg){height=80%}

##

![Edward Snowden, 2013](media/snowden.jpg){height=80%}

##

![Facial recognition](media/clearview-ai.jpg){height=80%}

##

![Deepfakes](media/deepfakes.jpg){height=80%}

##

![Théâtre D'opéra Spatial, Jason M. Allen, 2022](media/ai-art.jpg){height=80%}


##

!["John Dewey, Plato, and Jean Jacques Rousseau debating education in the Agora while Diogenes urinates on a statue in the background."](media/ai_plato_dewey.jpg){height=80%}

##

!["cicero and quintillian drunk in peristyle of roman domus"](media/ai_cicero_quintilian_statues.jpg){height=80%}

##

!["cicero and quintillian drunk in peristyle of roman domus"](media/ai_cicero_quintilian_bottles.jpg){height=80%}

##

!["cicero and quintillian drunk in peristyle of roman domus"](media/ai_cicero_quintilian_cup.jpg){height=80%}

##

![""Louis Armstrong discovering the swing beat, classic 
renaissance"](media/ai_armstrong.jpg){height=80%}

##

![""Louis Armstrong discovering the swing beat, classic 
renaissance"](media/ai_armstrong_arms.jpg){height=80%}

##

![""Louis Armstrong discovering the swing beat, classic 
renaissance"](media/ai_armstrong_fiddle.jpg){height=80%}

##

!["Etching of a jovial coffeehouse in 1700s England"](media/ai_coffeehouse.jpg){height=80%}

##

!["Etching of a jovial coffeehouse in 1700s England"](media/ai_coffeehouse2.jpg){height=80%}

##

!["Jonh Dewey and Mr. Rogers riding unicorns"](media/ai_dewey_rogers.jpg){height=80%}

##

!["Eching of Native American criticizing toleration of abject poverty in France"](media/ai_native_paris.jpg){height=80%}

##

!["Eching of Native American criticizing toleration of abject poverty in France"](media/ai_native_wolf.jpg){height=80%}

##

!["Children using Skinner's teaching machines in 1960s"](media/ai_teaching_machine.jpg){height=80%}

##

!["Children using Skinner's teaching machines in 1960s"](media/ai_teaching_machine2.jpg){height=80%}

##

!["Children learning in Dewey-style progressive school in 1920s"](media/ai_progressive_ed.jpg){height=80%}

##

!["Wacky professor teaches history of education at a Korean university"](media/ai_professor.jpg){height=80%}

##

!["A professor wearing a blue irish cap and a blue and yellow checker flannel shirt teaching about the history of education at a university in seoul. in the style of Vermeer"](media/ai_professor_koreans.jpg){height=80%}


##

!["A professor wearing a blue irish cap and a blue and yellow checker flannel shirt teaching about the history of education at a university in seoul. in the style of Vermeer"](media/ai_professor_manhwa.jpg){height=80%}

##

!["A professor wearing a blue flat cap and a gray and yellow checker flannel shirt teaching about the history of education at a university in seoul. the professor has no beard. drawn in the style of adventure time."](media/ai_professor_adventuretime.jpg){height=80%}
