---
title: Rights, Reason & Revolution
subtitle: "Week IX: The Eighteenth Century"
date: 2023-10-31
---



##

![Colonial Territories in 1700 [(CC-BY-SA Umanabha)](https://commons.wikimedia.org/wiki/File:1700_CE_world_map.PNG)](media/colonies_map_1700.png){height=80%}

##

![Cotton Gin](media/cotton_gin.jpg){height=80%}

##

![Spinning Jenny](media/spinning_jenny.jpg){height=80%}

##

![Watt Steam Engine [(CC-BY Nicolas Perez)](https://commons.wikimedia.org/wiki/File:Maquina_vapor_Watt_ETSIIM.jpg)](media/watt_steam_engine.jpg){height=80%}

##

![Screw Cutting Lathes](media/screw_lathe.jpg){height=80%}

##

![The Iron Bridge, UK [(CC-BY Andre Engels)](https://commons.wikimedia.org/wiki/File:Ironbridge_6.jpg)](media/iron_bridge.jpg){height=80%}

##

![Political cartoon, 1789, France](media/third_estate.jpg){height=80%}

##

![Storming of the Bastille](media/bastille.jpg){height=80%}

##

![Declaration of the Rights of Man](media/rights_of_man.jpg){height=80%}

##

![Declaration of the Rights of Man](media/rights_of_man_print.jpg){height=80%}

##

![The Declaration of Independence, 1776](media/declaration_independence.jpg){height=80%}

##

![Handbill advertizing slaves for sale](media/slavery_handbill.jpg){height=80%}

##

![Poster explaining slave stowage regulations](media/slave_ship.jpg){height=80%}

::: notes

12-13 million Africans shipped across Atlantic over span of 400 years

triangle trade: goods from england to trade for slaves, sell slaves in americas and buy goods, bring back to england to sell

:::

##

![British American Colonies in 1776 [(CC-BY-SA Golbez)](https://commons.wikimedia.org/wiki/File:United_States_Central_change_1776-07-04.png)](media/american_colonies_1776.jpg){height=80%}

##

![Macaroni Satire](media/macaroni.jpg){height=80%}

::: notes

- exaggeration of gentleman culture, emulation by non-aristocrats
- bourgeois excess
  - growing middle class, upper middle class
- consumer capitalism
  - excessively conspicuous consumption
  - fashion, food, etc.
- macaronic verse
  - irony is that the book of the courtier established gentlemanly behavior partly as the ability to use latin to create new words when needed!

:::

##

![Engraving of Harvard College, by Paul Revere](media/harvard.jpg){height=80%}

::: notes

- 71조 endowment
- 50 billion usd

- first printing press in us

:::

##

![West School, Massachusetts [(CC-BY John Phelan)](https://commons.wikimedia.org/wiki/File:West_School,_Burlington_MA.jpg)](media/west_school.jpg){height=80%}

##

![Old Ship Church, Massachusetts [(CC-BY-SA Michael Carter)](https://commons.wikimedia.org/wiki/File:InteriorOldShip.jpg)](media/old_ship_church.jpg){height=80%}


##

![The Treaty of Penn with the Indians by Benjamin West](media/west_treaty.jpg){height=80%}

##

![The Skater by Gilbert Stuart](media/skater.jpg){height=80%}

##

![Moonlight by Washington Allston](media/moonlight.jpg){height=80%}

##

![Benjamin Franklin](media/franklin_lightning.jpg){height=80%}


::: notes

- only schooled to 10 years old, and just read a lot of books
  - parents wanted him to be a clergyman, but they were poor. allowed him to drop out
  - Boston latin school, oldest school in usa

- love of books led into career as printer and publisher
  - realized the importance of free speech: silence dogood
  - the press is a public service; didactic...moral education of the masses
  
- junto: club for mutual improvement among tradesmen
  - based on coffehouses
  
- college of philladelphia (upenn)
  - elementa philosophica
  - new university model:
    - subject specialists instead of four years of resident tutoring (with same tutor)
    - english instead of latin
    - focus on professions
  - over 1/3 of declaration of independence signers were graduates or associates of upenn
  
- inventor, but against idea of patents
- proved lightning was electricity (coined terms positive and negative)
- first usage of word tofu in english (he was vegetarian)
- numerous honorary titles and degrees from various universities

:::

##


![Benjamin Franklin](media/franklin_fur.jpg){height=80%}

::: notes

- ambassador to france
- fur cap was a bold statement about american identity and genius

:::

##

![Pennsylvania Gazette](media/penn_gazette.jpg){height=80%}

##

![Political Cartoon by Benjamin Franklin](media/join_or_die.jpg){height=80%}

##

![Page from Newbery's *Pretty Little Pocket-Book*](media/pretty_pocketbook.jpg){height=80%}

##

![Title page of Encyclopedie](media/encycolpedia_title.jpg){height=80%}

::: notes

- society of people of letters
  - collective of intellectuals 
- contributions of all levels of quality
- most articles are anonymous
- not necessarily university faculty but public intellectuals


:::

##

![Frontispiece of the Encyclopedia](media/encycolpedia_frontispiece.jpg){height=80%}

::: notes


- central figure is truth (enlightenment)
  - philosophy and reason are removing the veil
- all the figures are very leisurely reading and learning
- hotbed for the expansion of deism: idea that god created world with rational and moral laws, but that he does not interfere in the world via miracles or revelation

:::

##

![Encyclopedia's Organization Scheme for Knowledge](media/encycolpedia_organization.jpg){height=80%}

::: notes

- notably includes the manual arts!

- influenced by bacon; representative of the trend to organize knowedge and make it accessible
- depicts the relationship of knowledge, but also provides way of researching
- in contrast to medieval times, logical arrangement...not just mired in vague categories of the liberal arts


:::

