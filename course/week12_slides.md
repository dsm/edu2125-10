---
title: "Week XII: The Twentieth Century" 
date: 2023-11-21
---

##

![Heroic Roses, Klee](media/klee_roses.jpg){height=80%}

##

![Burg und Sonne, Klee](media/klee_burg_sonne.jpg){height=80%}

##

![Ad Parnassum, Klee](media/klee_ad_parnassum.jpg){height=80%}

##

![My Room, Klee](media/klee_room.jpg){height=80%}

##

![View from the Dunes with Beach and Piers, Domburg, Mondrian](media/mondrian_dunes.jpg){height=80%}

##

![Tableau I, Mondrian](media/mondrian_tableau.jpg){height=80%}

##

![The Seine at Chatou, Vlaminck](media/vlamnick_seine.jpg){height=80%}

##

![le bonheur de vivre, Matisse](media/matisse_bonheur_vivre.jpg){height=80%}

##

![Dance, Matisse](media/matisse_dance.jpg){height=80%}

##

![Les Demoiselles d'Avingon, Picasso](media/picasso_demoiselles.jpg){height=80%}

##

![La guitare, Braque](media/braque_guitar.jpg){height=80%}

##

![Three Musicians, Picasso](media/picasso_musicians.jpg){height=80%}

##

![Guernica, Picasso](media/picasso_guernica.jpg){height=80%}

##

![Massacre of Korea, Picasso](media/picasso_korea_massacre.jpg){height=80%}

##

![The Old Guitarist, Picasso](media/picasso_old_guitarist.jpg){height=80%}

##

![Nude Descending a Staircase, Duchamp](media/duchamp_nude_stairs.jpg){height=80%}

##

![Fountain, Duchamp](media/duchamp_fountain.jpg){height=80%}

##

![LHOOQ, Duchamp](media/duchamp_lhooq.jpg){height=80%}


##

![Duchamp as Rrose Selavy](media/duchamp_rose_selavy.jpg){height=80%}

##

![Belle Heleine - Eau de Voilette, Duchamp](media/duchamp_belle_haleine.jpg){height=80%}

##

![Etant Donnes, Duchamp](media/duchamp_etant_donnes.jpg){height=80%}

##

![Guillame Apollinaire, 1902](media/apollinaire.jpg){height=80%}

##

![The Persistence of Memory, Dali](media/dali_persistence_memory.jpg){height=80%}

##

![Galatea of the Spheres, Dali](media/dali_galatea_spheres.jpg){height=80%}


##

![The Treachery of Images, Margritte](media/margritte_pipe.jpg){height=80%}


##

![The Empire of Light, Margritte](media/empire-of-light.jpg){height=80%}


##

![Art Deco/Futuristic 'Emerald City'](media/emerald_city.jpg){height=80%}

##

![NYC circa 1931](media/nyc_aerial_1931.jpg){height=80%}

##

![American Radiator Building, New York [(CC-BY Jean-Christophe Benoist)](https://commons.wikimedia.org/wiki/File:NYC_-_American_Radiator_Building.jpg)](media/radiator_building.jpg){height=80%}

##

![RCA Building under construction, 1933](media/rca_building.jpg){height=80%}

##

![Chrysler Building, New York](media/chrysler_building.jpg){height=80%}

##

![Chrysler Building, Lobby [(CC-BY Tony Hisgett)](https://commons.wikimedia.org/wiki/File:Chrysler_Building_Lobby.jpg)](media/chrysler_lobby.jpg){height=80%}

##

![Chrysler Building, Elevators [(CC-BY-SA Elisa.rolle)](https://commons.wikimedia.org/wiki/File:Chrysler_Building_01.JPG)](media/chrysler_elevator.jpg){height=80%}

##

![Fisher Building in Detroit](media/fisher_detroit.jpg){height=80%}

##

![Pan Pacific Auditorium, Los Angeles](media/pan_pacific_auditorium.jpg){height=80%}

##

![Austrians executing Serbians, 1917](media/serbian_execution.jpg){height=80%}

##

![Trench at Somme, 1916](media/somme_trench.jpg){height=80%}

##

![Influenza Pandemic](media/flu_pandemic.jpg){height=80%}

##

![Joe Barr at Kendell College, 1909](media/joe_barr.png){height=80%}

##

![Lula Fowler, 1909](media/lula_fowler.png){height=80%}

##

![Black Monday, 1929](media/black_monday.jpg){height=80%}

##

![Run on a NY bank](media/bank_run.jpg){height=80%}

##

![Bread Line in Chicago, opened by Al Capone](media/bread_line.jpg){height=80%}

##

![Damage of Dust Bowl in South Dakota](media/dust_bowl.jpg){height=80%}

##

![Vacant Farm, Texas](media/abandoned_farm.jpg){height=80%}

##

![Destitute family, Oklahoma](media/destitute_family1.jpg){height=80%}

##

![Destitute family, California](media/destitute_family2.jpg){height=80%}

##

![Woody Guthrie](media/guthrie.jpg){height=80%}

##

![Prohibition 1920](media/prohibition.jpg){height=80%}

##

![French Suffrage Poster, 1934](media/suffrage_poster.jpg){height=80%}

##

![Child laborers, 1908](media/baseball_kids_1908.jpg){height=80%}

##

![Child Labor Protestors](media/child_slavery.jpg){height=80%}

##

![8 year-old newsboy](media/newsboy.jpg){height=80%}

##

![Woman in factory, 1941](media/woman_factory.jpg){height=80%}

##

![Bombing of Hiroshima and Nagasaki](media/atomic_bombs.jpg){height=80%}

##

![Thermal Flash Burns](media/thermal_flash1.jpg){height=80%}

##

![Thermal Flash Burns](media/thermal_flash2.jpg){height=80%}

##

![London after blitz bombing, 1940](media/london_blitz.jpg){height=80%}

##

![Destruction of Warsaw, 1945](media/warsaw.jpg){height=80%}

##

![Unloading concentration camp corpses into mass grave, 1945](media/concentration_camp.jpg){height=80%}

##

![Robert Johnson](media/robert_johnson.jpg){height=80%}

##

![Louis Armstrong](media/satchmo.jpg){height=80%}

##

![Duke Ellington](media/duke.jpg){height=80%}

##

![Ella Fitzgerald](media/ella.jpg){height=80%}

##

![Charlie Parker and Miles Davis](media/bird_and_miles.jpg){height=80%}

