---
title: The Education of Pedagogues and Other Slaves in Ancient Greece
week: 2
topic: Ancient Greece
author: David Samuel Meyer
date: 2022-09-13
categories: protocols
description: the significance of educated slaves in ancient greece
tags:
- history
- education
- slavery
---

Last week we discussed some general concepts about history, "the west," and education. The notion of "the west" is a vague generalization, but is historically relevant if only for the fact that Europeans identified themselves within that category for centuries. Much of the known history of this "west" is the story told by men with the privilege to assert this identity, among other things. "Education" throughout the history of Europe, it will be found, pertains mostly to formal schooling of one form of another. The cultural heritage of this tradition is found in the "schools" of ancient Greece, which, ironically, were distinctive not because of their being formal institutions, but because of their methods of free inquiry and discussion.

# THE EDUCATION OF PEDAGOGUES AND OTHER SLAVES IN ANCIENT GREECE

`* * *`{=commonmark}

Slavery was commonplace in ancient Greece and throughout most of the ancient world. Some slaves were owned as property, and some were more like serfs bound to work on a piece of land, such as the helots of Sparta. They had absolutely no rights and were treated as subhuman. Plato explained that slaves should simply be put to work, disciplined, and fed. Aristotle expanded on this by recognizing that since slaves were, in fact, human beings they could understand instructions and recommendations for better behavior. The Stoic school of Zeno was the only school of Athens that allowed slaves to attend, in addition to women and other non-citizens. Typically, slaves received no education apart from learning to perform specific tasks, yet not only were slaves involved in the education of the young in an important way, there were a number of highly educated slaves who became important historical figures, such as Epictetus or Diogenes. How did these slaves become educated?

Some slaves may have already been literate prior to being enslaved. A literate citizen of another nation might become a slave in a Greek city-state as a result of war or sea piracy; like Diogenes the Cynic. It is likely that in many cases, however, especially in Athens where slaves were not treated so harshly, that educated slaves were self-taught. It was not uncommon for relatively "benevolent" masters to allow their slaves to save money for their own self-purchase, and evidence exists of slaves teaching themselves and each other, sometimes with the assistance of their master or members of their household. Owning a literate or educated slave would have been beneficial to a master if their intended work was not brute labor. In fact, it was not uncommon for slaves to oversee the education of the boys in the households of citizens.

These slaves were called *pedagogues*, which means literally, "boy guider." They were called pedagogues because their main job would be to escort boys to and from school. In Athens, children were not allowed to be out at night, and required escorts during the day to shelter them from the vulgarities of the streets and keep them out of trouble. Many pedagogues were actually illiterate, but there are many documented cases of highly educated pedagogues working as tutors to the children of welathy families. In spite of their being highly educated, such pedagogues were prohibited to participate in civic matters due to their slave status. It is curious that these slaves would have had no practical incentive to become educated. Perhaps these educated slaves better symbolize the spirit of Athenian learning than philosophers such as Plato, whose privilege afforded them esteem and prestige as compensation for their labor, so to speak.

## QUESTIONS

1. The typical model and methods of "school" today differ greatly from those of the Greeks, yet we still honor them as having embodied the spirit of humane civilization and democracy. We recognize free inquiry and discourse as having intrinsic value, yet our own schooling (even in higher education) seems unconcerned with these, methodologically speaking. How do the needs and aims of education in our society differ from that of the Greeks? How could contemporary education accommodate Greek educational methods, or could it not?

2. In Athens, it was not taboo for a slave to become educated. It was simply a matter of non-consequence since their slave status would disallow them from participating in civic activities. This reflects the mindset that education of the young had a primarily social purpose. In modern society, where basic education is available to everyone---and even compulsory---how does the relationship among social status, education and its aims differ from that of ancient Athens?
