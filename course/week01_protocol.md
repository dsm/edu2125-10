---
title: HISTORY AS THE RECURSIVE APPROPRIATION OF NARRATIVES
week: 1
topic: Orientation
date: 2022-09-06
categories: protocols
description: what does it mean to examine history?
tags:
- education
- history
---

Yesterday, we went over the syllabus and reviewed the course format, assignments, grading policy, schedule, etc. On Tuesdays, the instructor will lecture and present various multimedia pertaining to the art, culture, lifestyle, and historical background of the time period discussed that week. Wednesday classes will consist of group discussions. Before each class participants will write and submit a protocol in preparation for these discussions. 

# HISTORY AS THE RECURSIVE APPROPRIATION OF NARRATIVES


The literal meaning of "history" is "a story." As we look back on the histories we have received, we may wonder whose stories they tell, who are telling them, and what they are based upon.  Like with any story, fictional or otherwise, history is selective and interpretive. There cannot be a complete history that tells all there is to know about what happened: every fact, every event, every experience, every perspective. This is significant for several reasons. First, it is impossible to know everything or even comprehend the vast complexity of all the concrete dynamics of every single life lived---all the desires, fears, ideals of every person that contributed to the actual, material conditions of the world. Second, even if it was possible to somehow *capture* and *examine* every single detail of every single thing that happened and was experienced over a given period of time, the experience of analyzing and telling *that* story is distinct from the experience of actually living it---and nobody can live all lives at once! Entropy is inherent to time, and time is inherent to existence.

[Is it possible to replicate the conclusions of history?]{.aside}


The story a history tells, then, is always selective and interpretive, not only because its sources are inherently limited, but because it is an expression of what those sources *mean*; what their perceived significance is. The significance of the past is always understood in terms of the present *and* the future. This "story" or narrative that history weaves through the facts available to it are not merely a record of every known detail, but the imaginative construction of a perspective which can *make sense* of what is known about the past. This *sense making* appropriation of the meanings of the past is rooted in the interests and habits of the present day---in an *ethos*---which is necessarily concerned with continuing to exist into the future, or at least anticipating it. Reading the past is always, in part, oriented by this background sense of where we think we are and where we think we are going.


[How has history been approached differently by different peoples in different time periods?]{.aside}

Every generation looks back on its history not simply to *observe* what has already happened, but to understand where and how it exists currently; to establish its identity. The process is recursive: history develops through the process of histories retelling histories retelling histories etc. Individual students of history may note its striking monophonic quality: that the tellers and those whose stories they tell are those who have "won" their privilege to do so. Looking back on history, it behooves us to listen for the voices of the marginalized and to add our own to the developing human drama.

## QUESTIONS

1. The location and identity of "the west" is difficult to place; geographically, culturally, and politically. Whose story does the "history of western education" tell and who tells it? Whose voices and identities (of the past and present) are included and excluded in that narrative?

2. In the hyper-connected globalized "informational age," is the east-west dichotomy relevant anymore? Was it ever?

3. Why should we be interested in the history of "the west" as opposed to the history of other old civilizations such as India, Persia, or China?
