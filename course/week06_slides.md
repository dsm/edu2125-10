---
title: Cultural Revolution in 16th Century Europe
subtitle: "Week VI: The Reformation & The Growth of Humanism"
date: 2023-10-10
---

# Martin Luther & The Reformation

##

::: notes

- attended a school run by bretheren of common life
  - later said it was like hell and purgatory
- univrestiy of erfurt
  - called it a beerhouse and a whorehouse
  - rote learning and spiritual exercises
  - masters degree
- studied phliosophy, but was unsatisfied. he was very religious
- nearly struck by lightning and decided to become a priest (his dad saw this as a waste of his education)
  - as a monk he was almost torturous to himself, ordered to pursue an academic career
- taught theology at uni of wittenberg, eventually became the chair
- 95 theses: "disputation on the power and efficacy of indulgences"
- the nailing of the theses to the door:
  - some say he never did, that he was not in town at time
  - some say he did, but that it was not a dramatic act like history remembers. it was common practice to "publish" your disputation this way so others could prepare for the colloquium

:::

::: columns

:::: column

![Martin Luther](media/martin_luther.jpg){height=80%}

::::

:::: column

![95 Theses](media/95theses.jpg){height=80%}

::::

:::

##

![Satan selling indulgences](media/satan_indulgences.jpg){height=80%}

##

![Luther's Translated Bible](media/luther_bible.jpg){height=80%}

# Print Media & Popular Culture

##

![Broadside of Luther, 1617](media/luther_broadside.jpg){height=80%}

##

![Christ's forgiveness outweights indulgences from Pope](media/broadside_christ_forgiveness.jpg){height=80%}

##

![From "One Can Be Saved Without the Indulgence of Rome"](media/broadside_indulgence_selling.jpg){height=80%}

##

::: notes

- on literacy:
  - for centuries, paper was very expensive. written word was not as important or available in medieval society as it was in ancient rome and greece
  - mostly for religion, limited access, latin works etc
  - vernacular languages were rarely written down, and untill the high middle ages and the renaissance, the written form of these languages were still not standardized
- one effect of reformation is the movement toward vernacular literacy
- printed media completely changed everything, because written word appeared in society in numerous forms, more available and more variety
  - difference between reading and writing (and copying); also vernacular or latin literacy (also french)

:::

![Ballad of Robin Hood](media/broadside_robin_hood.jpg){height=80%}

##

![c. 1570 [(CC-BY-NC Huntington Library - Britwell HEH 18295, EBBA 32227)](https://ebba.english.ucsb.edu/ballad/32227/image)](media/broadside_northumberland_news.jpg){height=80%}

##

![1555-1584 [(CC-BY-NC Huntington Library - Britwell HEH 18324, EBBA 32411)](https://ebba.english.ucsb.edu/ballad/32411/image)](media/broadside_london_warning.jpg){height=80%}

##

![1570 [(CC-BY-NC Huntington Library - Britwell HEH 18344, EBBA 32584)](https://ebba.english.ucsb.edu/ballad/32584/image)](media/broadside_flood.jpg){height=80%}

##

::: columns

:::: column

![Thumbnail Sketch](media/rudolph_thumbnail.jpg){height=80%}

::::

:::: column

![Engraving of Rudolph II by Saedeler](media/rudolph_engraving.jpg){height=80%}

::::

:::

##

![Christ Appearing to Mary Magdelene as a Gardener, by Spranger ](media/spranger_christ_mary.jpg){height=80%}

##

![Venus and Cupid, by Goltzius](media/goltzius_venus.jpg){height=80%}

##

![Icarus, by Goltzius](media/goltzius_icarus.jpg){height=80%}

##

::: columns

:::: column

![Edmund Spenser](media/spenser.jpg){height=80%}

::::

:::: column

![William Shakespeare](media/shakespeare.jpg){height=80%}

::::

:::

# Mannerism

##

![The Seven Liberal Arts, by de Vos](media/devos_liberal_arts.jpg){height=80%}

##

![Hercules, Deianira, Nessus, by Spranger](media/spranger_hercules.jpg){height=80%}

##

::: notes

- female nudity; notable that minerva is semi-nude, she never was for the greeks
- symbols of fertility and abundance in renaissance art generally
- significant that nude female form emerges as motif in "humanist" art

:::

![Minverva Victorious over Ignorance; Spranger](media/spranger_minerva.jpg){height=80%}

##

![Garden of Love, van Mander](media/vanMander_garden.jpg){height=80%}

##

![The Peasant Wedding, Brueghel](media/brueghel_peasant_wedding.jpg){height=80%}

##

::: notes

- reimagination of religion in european setting

:::

![Sermon of Saint John the Baptist, Brueghel](media/brueghel_sermon.jpg){height=80%}

##

![Flower Still Life, Bosschaert](media/bosschaert_still_life.jpg){height=80%}

##

![Platter by Palissy](media/palissy_platter.jpg){height=80%}


# The Great Iconoclasm (Beeldenstorm)

##

![Calvinist Iconoclastic Riot, by Hogenberg](media/iconoclasm.jpg){height=80%}

##

![Cathedral of Saint Martin, Utrecht](media/utrecht_iconoclasm.jpg){height=80%}

##

![St. Medardus Church in Wervik [(CC-BY-SA Dgalle)](https://commons.wikimedia.org/wiki/File:Sint-Medarduskerk_Wervik._Sporen_van_de_beeldenstorm..JPG)](media/wervik_iconoclasm.jpg){height=80%}

##

![Beeldenstorm Broadside](media/beeldenstorm_broadside.jpg){height=80%}


##

![Bildersturm Broadside](media/bildersturm_broadside.jpg){height=80%}

::: notes

nobles
gentry & merchants
yeoman and craftsman, often could read and write
tenant farmers, wage laborers

vagabonds, law that prohibited not having a job

- whipped in public as punishment
- then forced to return to hometown
- problem was so "serious" that for a brief time, punishment was slavery for two years

theatre was becoming popular, but government did not like actors

* considered unuseful; had to have a license granted by a noble after 1572
* otherwise they would be arrested as vagrants

* before theatres, performed in marketplaces, squares
* built theatres; poor people stood in open air, rich people had covered seats, and the richest could sit on stage
* boys played parts of women
* usually during day time

* school started at 6am in summer and 7am in winter, finished ata bout 5pm. boys went to school 6 days a week, few holidays
* if you didn't go to school, you would hope to get an apprenticeship
* only rich and middle class girls were educated, either at home by mom or by a tutor
* married by 15-16 for girls, 18-21 for boys. rich kids' marriages were arranged

:::

##

![Fancy Doublet](media/doublet.jpg){height=80%}

##

![The Tailor, by Giovanni Battista Moroni](media/the_tailor.jpg){height=80%}

##

![Jerkin, Trousers & Hose](media/jerkin1.jpg){height=80%}

##

![Sir Walter Raleigh & Son](media/jerkin2.jpg){height=80%}

##

![Leather Jerkin [(CC-BY-SA Manx National Heritage)](https://commons.wikimedia.org/wiki/File:Picture_of_jerkin.jpg)](media/jerkin_leather.jpg){height=80%}

##

![Cloak [(CC-BY-SA VAwbteam)](https://commons.wikimedia.org/wiki/File:Cloak.jpg)](media/cloak.jpg){height=80%}

##

![Overgown](media/overgown.jpg){height=80%}

##

![Dress & Farringbone](media/tudor_dress.jpg){height=80%}


##

![Silk & Baleen Corset](media/corset.jpg){height=80%}

##

![Curled Hair & Fancy Hat](media/tudor_headwear.jpg){height=80%}

##

![Vagabond](media/bosch_vagabond.jpg){height=80%}

##

![Peasant Clothing](media/peasant_clothing.jpg){height=80%}

##

![Duckbill Shoe](media/duckbill_shoe.jpg){height=80%}


##

![Hardwick Hall [(CC-BY-SA Spinster)](https://commons.wikimedia.org/wiki/File:Hardwick_Hall_in_Doe_Lea_-_Derbyshire.jpg)](media/hardwick_hall.jpg){height=80%}

##

![Tudor House [(CC-BY-SA Peter Trimming)](https://commons.wikimedia.org/wiki/File:Tudor_House,_Southampton.jpg)](media/tudor_house.jpg){height=80%}

##

!["The Maiden" [(CC-BY-SA Kim Traynor)](https://commons.wikimedia.org/wiki/File:The_Maiden,_National_Museum_of_Scotland.jpg)](media/guillotine.jpg){height=80%}

##

![Drawing & Quartering](media/drawn_quartered.jpg){height=80%}

##

![Pillory](media/pillory.jpg){height=80%}

##

![Stocks](media/stocks.jpg){height=80%}

##

![Burning at the Stake](media/burning_stake.jpg){height=80%}

##

![The Bear Garden & Globe Theatre](media/globe_and_bear.jpg){height=80%}

##

![Globe Theatre, Cross Section [(CC-BY-SA C. Walter Hodges)](https://commons.wikimedia.org/wiki/File:Hodge%27s_conjectural_Globe_reconstruction.jpg)](media/globe_theatre.jpg){height=80%}

##

![Hornbook [(CC-BY-SA Geneus01)](https://en.wikipedia.org/wiki/File:Horn_book_front_\(hires\).jpg)](media/hornbook.jpg){height=80%}

##

![The Four Humours](media/humours.jpg){height=80%}
