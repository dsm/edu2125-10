---
title: Hellenistic vs. Cynic Cosmopolitanism
topic: The Dispersion of Greek Education
week: 2
author: David Samuel Meyer
date: 2022-09-13
categories: protocols
description: contrasting notions of personhood in ancient greece
tags:
- history
- education
- cosmopolitanism
- cynicism
---

During the last class (Thu, Sep 8), we looked at some slides of ancient Greek artworks to get a feel for the spirit of ancient Greek culture. The climate of Greece encouraged outdoor gatherings and the development of various types of special outdoor gathering places which became the historic cornerstones of their civil society. The Greeks' notion of fine art pertained to art forms that were public---which not only expressed the civic spirit, but which physically and culturally shaped the civic landscape. These included architecture, sculpture, and theatre (music, poetry, &c.). The zenith of art for the Greeks, generally speaking, was the expression of human ideals and emotions, especially as they pertained to the common experience of the polis.

# HELLENISTIC VS. CYNIC COSMOPOLITANISM

`* * *`{=commonmark}

During the Hellenistic Period, Greek culture was disseminated all across the Mediterranean and West Asia. One effect of this was a kind of cosmopolitanization of Greek culture. If ancient Greeks identified primarily with their polis, or perhaps more broadly with the Hellas ethnicity, then we may say that during the Hellenistic Period the "kosmos," or "world," displaced the polis as the most general category for collective identity. This new "world" was of course a Greek one, and although it was adapted and appropriated in as diverse ways as the peoples over whom it gained influence, this world was an empire of Greek colonies. The Greek polis had transcended itself to become a "polypolis." It is interesting to consider how this notion of cosmopolitanism differs from the cynical view of Diogenes, who is credited with having -coined the term. 

Diogenes (412 - 323 BCE) was a contemporary of Plato during the classical period of ancient Greece. When asked where he was from he responded by referring to himself as a citizen of the world (kosmopolitês). His world, however, was not a Greek empire, but *the* kosmos, or the world at large. Diogenes' view is noteworthy not just for this minor semantic difference, but because his philosophy and behavior represent a highly critical view of Greek civilization and the cult of the polis, and is completely at odds with the "cosmopolitanism" of the Hellenistic Period.

After attempting to debase the currency of his native land, Sinope, Diogenes allegedly came to Athens to debase the social norms and values of Greek society. He lived an ascetic lifestyle with no luxuries and as few comforts and possessions as possible. He was famous for doing obscene and awkward things in public, such as eating in the marketplace, masturbating in public, defecating during theatre performances, and disrupting Plato's school with witty rebuttals to Plato's theories, comical insults, and other anti-social behavior. There is a famous account of him meeting Alexander the Great as an old man. Alexander, who was a well-learned pupil of Aristotle, and then the Emperor of Macedonia, was excited to meet the famous Diogenes. He found Diogenes studying a pile of human bones, who then explained, "I am looking for the bones of your father but I can't tell them apart from those of a slave."

Diogenes' views represent those of the underdog, which is a fitting term since the word "cynic" comes from the Greek word for dog. He reputedly observed the simple and harsh lives of the poorest residents of the city and forced himself to live no better than they---to be content with nothing more than they had. Humanity, civilization, virtue, morality, and the world as a whole would appear differently to an aristocrat than they would to an ascetic living out of a clay pot. Although none of Diogenes' works have survived, his ideas and the numerous anecdotes about him provide an alternative view of personhood and humane values in Greek society---a view into the shadows of the Acropolis, so to speak. This cynical viewpoint is also significant because its followers later established the Stoic school of philosophy, whose notion of philosophy as a way of life had a tremendous influence on the austerely practical Romans and the Roman Empire.

## QUESTIONS

1. The ideas that drove the Hellenization of the ancient world were largely those of the Athenian aristocracy. How does the typical Athenian notion of the common human experience differ from that of a Cynic? How might the "cosmopolitan" education of the Hellenized world have been different had it adopted a Cynic or Stoic perspective?

2. What was education like for the typical "citizen of the world" during the Hellenistic Period? What differences or similarities are there between contemporary cosmopolitanism, such as the notion of "global citizenship," and that of the Hellenized world? Do education and cosmopolitanism affect each other in any significant way?
