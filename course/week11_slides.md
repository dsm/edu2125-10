---
title: "Week XI: The Nineteenth Century" 
date: 2023-11-13
---

##

![fin de siècle](media/fin_de_siecle.jpg){height=80%}

##

![World Fair Advertisement](media/world_fair_1900.jpg){height=80%}

##

![Champs des Mars](media/champs_de_mars.jpg){height=80%}

##

![Statue of Liberty at Paris World Fair, 1878](media/liberty_head.jpg){height=80%}

##

![1896 Olympics Report](media/1896_olympics.jpg){height=80%}

##

![1896 Olympics, participating countries](media/1896_olympics_map.jpg){height=80%}

##

![Olympic Cyclists](media/olympic_cyclists.jpg){height=80%}

##

![Olympic Tug-Of-War](media/olympic_tugofwar.jpg){height=80%}

##

![Video Telephony in 2000](media/video_telephony.jpg){height=80%}

##

![projection cinema](media/cinematographe_lumiere.jpg){height=80%}

##

![Madame Monet and Her Son](media/monet_madamme.jpg){height=80%}

##

![Irises, Van Gogh](media/van_gough_irises.jpg){height=80%}

##

![The Sower, Van Gogh](media/van_gough_sower.jpg){height=80%}

##

![Red Vineyards, Van Gogh](media/van_gogh_red_vineyards.jpg){height=80%}

##

![La Nuit étoilée](media/starry_night.jpg){height=80%}

##

![Kop van een skelet met brandende sigaret](media/van_gogh_skeleton.jpg){height=80%}

##

![Femmes de Tahiti, ou Sur la plage](media/gaugin_tahitian_women.jpg){height=80%}

##

![still life with curtain, cezanne](media/cezeanne_still_life.jpg){height=80%}

##

![pyramid of skulls, cezanne](media/cezeanne_skulls.jpg){height=80%}

##

![The Football Players](media/rousseau_footballers.jpg){height=80%}

##

![Zodiac, Mucha](media/mucha_zodiac.jpg){height=80%}

##

![The Arts: Painting, Mucha](media/mucha_painting.jpg){height=80%}

##

![The Scream, Munch](media/munch_scream.jpg){height=80%}

##

![La Goulue, Toulouse-Lautrec](media/toulouse-lautrec-goulue.jpg){height=80%}

##

![At the Moulin Rouge, Toulouse-Lautrec](media/toulouse-lautrec-moulin_rouge.jpg){height=80%}

##

![Le Chat Noir, Toulouse-Lautrec](media/toulouse-lautrec-chat_noir.jpg){height=80%}

##

![Charles Darwin](media/darwin.jpg){height=80%}

##

!['Anthropological' Exhibit Advertisement](media/human_zoo_add.jpg){height=80%}

##

![Ota Benga](media/ota_benga.jpg){height=80%}

##

![Brussels World Fair, 1958](media/brussels_human_zoo.jpg){height=80%}

##

![Mutilated Congolese Subjects](media/mutilated_congolese.jpg){height=80%}

##

![Abraham Lincoln](media/lincoln.jpg){height=80%}

##

![anti-immigration cartoon](media/1890_immigration.jpg){height=80%}

## From "The New Colussus"

```
Give me your tired, your poor,
Your huddled masses yearning to breathe free,
The wretched refuse of your teeming shore.
Send these, the homeless, tempest-tost to me,
I lift my lamp beside the golden door!
```

##

![Manhattan, 1876](media/manhattan_1876.jpg){height=80%}

##

![Little Italy, NYC, 1900](media/little_italy_1900.jpg){height=80%}

##

![Five Cents a Spot](media/ny_tenement.jpg){height=80%}

##

![Walt Whitman](media/walt_whitman.jpg){height=80%}

##

![Leaves of Grass Frontispiece](media/walt_whitman_engraving.jpg){height=80%}

##

![John Dewey as a young man](media/john_dewey_young.jpg){height=80%}

##

![John Dewey ca. 1930s](media/john_dewey_old.jpg){height=80%}
