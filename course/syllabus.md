---
title: Syllabus
course: History of Western Education
sched: "Tues 12--2pm; Wed 12--1pm"
room: "교603"
lecture: Wednesday
discussion: Tuesday
institute: Yonsei University
coursenum: EDU2125
location: Seoul
college: "Department of Education Sciences"
semester: Fall 2024
lang: en-US
description: syllabus for the course history of western education
categories: syllabi
tags:
- history
- education
summary: This course surveys the main developments in education from ancient Greece up until present day, emphasizing in particular the cultural and philosophical milieu of each place and period.
intro: This course surveys the main developments in education from ancient Greece up until present day, emphasizing in particular the cultural and philosophical milieu of each place and period. Participants will develop a critical understanding of the historical dynamics through which educational theory and practice evolved in “the west” and the influence of these on contemporary education.
---

# Overview

## Participants' Responsibilities

1. Attend 15 weeks of class.

2. Each week:

    - read *one or more* of the assigned readings
    - complete an *ungraded survey* online about the assigned readings
    - submit one *discussion question* about the weekly topic
    - discuss the weekly topic with several of your classmates
    
3. Write an essay in response to writing prompts about the *themes discussed in class* during midterms and finals weeks.

\
\

## Instructor's Responsibilities

1. Clearly communicate the criteria and results of evaluation, and provide participants with the opportunity to appeal the instructor's assessment of their work.

2. Curate a collection of readings and resources to adequately introduce the main topics in the history of education in Europe and the Americas.

3. Engage participants with insightful lectures, discussions, and multimedia presentations, and make these accessible in various formats.

4. Adapt the course contents to meaningfully accommodate the interests and curiosities of participants as expressed in their survey responses, discussion comments, questions, etc.

## Objectives

1. Understand and appreciate the distinct aesthetics, ideals, and cultural dynamics which influenced the education of each period and place.

2. Critically investigate the canonical narratives of western history and its major controversies.

3. Evaluate the numerous ways education has been and could be undertaken.

# Format

[All course materials and lectures will be in English. The English proficiency of students will not be evaluated.]{.aside}

Every week participants will read one or more of the assigned readings and complete a brief survey online before the start of class on Tuesday. The instructor will adjust the lecture and course contents based on participants' responses to the survey questions. Participants will discuss their thoughts on the weekly topic with other class members at the beginning of class every Tuesday, followed by a lecture session in which the instructor will answer questions and elaborate on the topics and themes of the readings. 

Class discussions and lectures will not merely summarize or restate the content of the weekly reading. Class participants are not expected to memorize or master the content of the reading, but rather to think about it and formulate their own questions. The purpose of the class discussion, likewise, is not for participants to prove they have read and can recall the content of the reading materials. Discussions are an opportunity for participants to share and discuss their thoughts about the topic to clarify and enrich their own and everyone else’s understanding. Furthermore, class discussions are not intended to be debates. Participants are not expected to develop and defend formal arguments. Think of class discussions more as a kind of wondering out loud.

## Tuesday Sessions

[This course will not accommodate the discrimination of gender, race, or class, and will not tolerate any form of hate speech or harassment. Just be kind to one another.]{.aside}

Before the start of class every Tuesday, participants will complete a brief online survey. Tuesday sessions will begin with small-group discussions in which participants will share their comments, questions, and curiosities on the topic of that week. Discussion groups will be randomly assigned each week. 

Participants are encouraged to ask questions---in and outside of class. The more clearly participants can identify and express their confusions, criticisms, curiosities. etc., the more fruitful the class discussions and lectures may become. After small-group discussions conclude, the instructor will attempt to address participants' questions, facilitate further discussion, and elaborate on and supplement the content of the readings.

## Wednesday Sessions

Wednesday sessions will consist of multimedia presentations about significant works of art, cultural artifacts, customs, lifeways, etc. from the places and time periods to be discussed the following week. These special sessions are meant to supplement the weekly readings by providing participants with a sense of the context and dynamics in which significant historical developments took place.


# Weekly Surveys

Before the start of class every Tuesday, participants will complete an online survey pertaining to the topic that week. In each survey, participants will:

1. Indicate which of the assigned readings they read that week;

2. Complete a brief survey consisting of questions about the various concepts, terminology, events, people, themes, etc. addressed in the readings. **Participants' responses to the survey questions will not be graded.** The purpose of the survey is not to evaluate participants' comprehension of the readings, but to help the instructor adjust the contents of the lecture and better facilitate class discussions;

3. Submit one *discussion question* about something that is unclear, confusing, or interesting about the contents of the readings or weekly topic. While participants' quiz responses will not be evaluated, the quality and sincerity of discussion questions will be. (See the [Evaluation Policy](#evaluation-policy) below.);

[The use of generative AI to complete quizzes or produce discussion questions is not allowed.]{.aside}

The deadline to complete the weekly survey will be strictly observed. Late submissions will not be accepted.


# Written Assignments

Participants will write two essays over the course of the semester---one during midterms week, and one during finals week.

Writing prompts will be provided before the beginning of midterms and finals weeks. Participants will select from among these and write and essay in response to them. Writing prompts will pertain to the *topics and themes discussed in class* rather than the contents of the readings.

Essays will not be generic reports about historical events, topics, etc. Rather, they will be speculative and reflective in nature, expressing participants' interpretations of the themes discussed in class. There will be no minimum length requirement, but essays should not be excessively long or short. Essays will be evaluated based on their quality, not their quantity. (Submitting a 20 page essay will not ensure you a good grade, for example).

To prevent plagiarism and the use of generative AI, essays will be submitted to TurnitIn (via LearnUs). Per university policy, participants found guilty of plagiarism will automatically fail the course. Students must clearly cite their sources where necessary. Quotations that are not cited will be flagged by TurnitIn, and authoritative claims unsubstantiated by cited evidence may result in additional scrutiny and point deductions.

Essays may be written in either Korean or English---or a mixture of both. 

Participants will be automatically enrolled for this course on TurnitIn using the email address they use for university communications (the email address on the course roster). Each participant will receive an email from TurnitIn to confirm their account details.

# Evaluation Policy

**Students will not be evaluated based on their English proficiency.**

## Attendance: 20%

**Attendance will be calculated as the percentage of total course hours attended.**

Students will be considered tardy after ten minutes, and absent after thirty. Per university policy, students absent for more than one-third of the total number of class hours will automatically fail the class.

University attendance policies will be strictly observed. Roll will be called at the beginning of every class session (until the instructor learns everyone's name). Absences excusable under university policy may be formally excused by providing the relevant documentation (refer to the academics manual for more information).

## Surveys: 20%

**Credit will not be given for partially completed surveys**. The contents of participants' responses to the reading and survey sections will not be evaluated, but their completion is mandatory. The *discussion questions* that participants submit each week will be the primary focus of evaluation, based on the following criteria:

1. Is the question written in a way that reasonably facilitates discussion? (That is, is it open-ended and not a "yes-or-no" question?)

2. Does the question demonstrate that the participant has read and reflected on the readings or sufficiently wondered about the topic?

3. Is the question sincere? (That is, it should not be an afterthought!) Even if you thought the reading or topic was uninteresting, you can still write a question that expresses your disinterest.

Late survey submissions will not be accepted and may not be made-up.


## Written Assignments: 40%

Late submissions will suffer a 20% point deduction for each day they are submitted past the deadline. Essays will not be evaluated based on their length, but on the quality of their content, according to the following criteria:

1. Does the response demonstrate a sufficient understanding of the prompt's context and significance? Does it adequately address the prompt's main points?

2. Is the response coherent and reasonable?

3. Does the response adequately justify its assertions? Does the author substantiate their claims by clearly citing their sources?

4. Does the essay represent the view of someone who has sufficiently engaged with the course contents? That is, does the essay express the author's own insights and interpretations, or is it a generic report on historical topics?



## Participation: 20%

**Participation will be evaluated as the participant's overall level of engagement in the course**. Points may be deducted from one's participation score for the following reasons:

1. Excessive absence or tardiness.

2. Declining to participate in group discussions.

3. Bigotry, verbal abuse, general disrespect, etc.

4. Excessive failure to complete weekly surveys.

5. Not taking an exam, or not taking it seriously.

6. Not engaging with the course materials posted on LearnUs.

7. Cheating, plagiarism, etc.

## A Note About English Skills and the Use of Translators

This class is an English course. All the readings, presentations, and lectures will be in English. If you feel that this may be too burdensome for you, then you might want to reconsider enrolling in this class. However, **please do not be intimidated if you are not totally confident in your English skills**!

The English proficiency of participants will not be evaluated, but all assignments should be coherent and legible. Even though English writing skills are not evaluated in this class, **the use of generative AI such as ChatGPT to produce English translations will not be allowed**. The use of ChatGPT to cheat has become a major problem, and its use as a tool to plagiarize is sometimes indistinguishable from its use as a translator. Additionally, ChatGPT produces sterile and bland translations that effectively eliminate your own voice and personality from the translated text. For this reason and others, the use of any translator, such as DeepL, is also discouraged.

**Even if your English composition is not very refined, I would prefer to read that over a bland, generic translation produced by AI**! If you feel that your writing is not clear enough, then please do not hesitate to write entirely or partially in Korean. The amount of writing that you will need to do for this class is relatively small, so I encourage you to take it as an opportunity to challenge yourself. 

To reiterate, survey responses and essays written in Korean will be accepted and will not be penalized. When communicating with the professor in person or by email, the use of either English or Korean is perfectly acceptable.

Lastly, in small-group discussions it is not necessary to speak in English among yourselves (so long as the other group members are not excluded by your use of another language; be it Korean, French, Chinese, etc.).


# Materials

**All reading materials will be provided in electronic formats via LearnUs.**

The readings for this course will mostly consist of selected chapters from the sixth edition of William Boyd’s *The History of Western Education* and James Bowen’s *A History of Western Education*. The instructor may provide other supplementary reading materials such as articles or lectures. Each week, participants will select one or more of the assigned readings to read before the start of class on Tuesday.

Participants can expect to read between twenty to fifty pages per week.

[A Korean translation of Boyd's book exists and is available in the library.]{.aside}


\
\

# Agenda

## WEEK 1: SEPT 2–8
ORIENTATION

- Sept 3---Lecture: *Course Introduction*

- Sept 4---Presentation: *The Greek Aesthetic*
  
  

## WEEK 2: SEPT 9–15
ANCIENT GREECE AND ITS LEGACY

- Readings:

  1. Boyd Ch 01 - Greek Education
  2. Boyd Ch 02 - The Dispersion of Greek Education
  3. Bowen Vol1 Ch 03 - Early Civilization of Hellas
  4. Bowen Vol1 Ch 04 - The Achievement of Athens
  5. Bowen Vol1 Ch 05 - Rhetoric and Philosophy
  6. Bowen Vol1 Ch 06 - Aristotle and Higher Learning in Hellenistic Athens
  7. Bowen Vol1 Ch 07 - The Hellenistic Orient

- Sept 10---Discussion

- Sept 11---Presentation: *Liberty, Humanity, Urbanity*




## WEEK 3: SEPT 16–22
THE RISE AND FALL OF EDUCATION IN THE ROMAN EMPIRE

[**Chuseok Holiday 9/16--9/18**]{.aside}

- Readings:

  #. Boyd Ch 03 - Education in the Roman Empire
  #. Bowen Vol1 Ch 08 - Republic of Rome
  #. Bowen Vol1 Ch 09 - Empire of Rome
  #. Bowen Vol1 Ch 10 - Religious Conflict and Syncretism
  #. Bowen Vol1 Ch 11 - Foundations of Christian Education
  

- Sept 17---Discussion

  (*Online Class --- To Be Announced*)

- Sept 18---Presentation: *The Aesthetics of the Early Middle Ages*

  (*Video presentation to be viewed online in lieu of class*)
  
  
  
  

## WEEK 4: SEPT 23–29
EDUCATION IN THE EARLY MIDDLE AGES

- Readings:

  #. Boyd Ch 04 - The Dark Ages
  #. Bowen Vol1 Ch 12 - Preservation of Traditional Learning
  #. Bowen Vol1 Ch 13 - The Christian Church and Western Learning
  #. Bowen Vol2 Ch 01 - Foundations of European Education

- Sept 24---Discussion

- Sept 25---Presentation: *The Aesthetics the High Middle Ages*
  
  
  

## WEEK 5: SEPT 30–OCT 6
THE EMERGENCE OF UNIVERSITIES


- Readings:

  #. Boyd Ch 05 – The Rise of the Universities
  #. Bowen Vol2 Ch 02 - Era of the Cathedral Schools
  #. Bowen Vol2 Ch 03 - An Age of Crisis 1150--1230
  #. Bowen Vol2 Ch 04 - Emergence of the University
  #. Bowen Vol2 Ch 05 - Age of Scholasticism

- Oct 1---Discussion

  [**Temporary Holiday**]{.aside}
  
  (*Video presentation to be viewed online in lieu of class*)

- Oct 2---Presentation: *The Renaissance Humanistic Ideal*
  
  
  
  

## WEEK 6: OCT 7–13
THE RENAISSANCE AND HUMANISTIC EDUCATION

- Readings:

  #. Boyd Ch 06 – Humanistic Education
  #. Bowen Vol2 Ch 06 - Italy: Classical Revival of the Trecento
  #. Bowen Vol2 Ch 07 - Italy: Humanism of the Quattrocento
  #. Bowen Vol2 Ch 08 - Italy: Higher Learning in the Quattrocento
  #. Bowen Vol2 Ch 09 - Expansion of Education and Humanism: France and Germany to 1500
  #. Bowen Vol2 Ch 10 - Expansion of Education and Humanism: England to 1500

- Oct 8---Discussion

- Oct 9---Presentation: *Cultural Revolution in 16th Century Europe*
  
  [**Hangul Day**]{.aside}
  
  (*Video presentation to be viewed online in lieu of class*)





## WEEK 7: OCT 14–20
THE REFORMATION AND THE GROWTH OF HUMANISM

- Readings:

  #. Boyd Ch 07 - The Reformation and Education
  #. Boyd Ch 08 - The Broadening of Humanism
  #. Bowen Vol2 Ch 11 - Christian Humanism: Desiderius Erasmus and the Ideal of Piety
  #. Bowen Vol2 Ch 12 - Christian Humanism: Martin Luther and the Reformation in Germany
  #. Bowen Vol2 Ch 13 - Extension of Educational Thought and Practice in the Sixteenth Century
  #. Bowen Vol2 Ch 14 - The Search for Method: Toward a Ratio

- Oct 15---Discussion

- Oct 16---Presentation: *Power, Influence, Wealth, and "Light"*
  
  
  
  

  
## WEEK 8: OCT 21–27
MIDTERMS

- Oct 22---No Class

- Oct 23---No Class

**First essay due by October 28th at 00:00 (midnight)**




## WEEK 9: OCT 28–NOV 3
THE SEVENTEENTH CENTURY

- Readings:

  #. Boyd Ch 09 – The Seventeenth Century
  #. Bowen Vol3 Ch 02 - The Scientific Revolution of the Seventeenth Century
  #. Bowen Vol3 Ch 03 - Utopia and Reality in the Seventeenth Century
  #. Bowen Vol3 Ch 04 - The Catholic Conservative Tradition
  #. Bowen Vol3 Ch 05 - The Protestant Initiative
  #. Bowen Vol3 Ch 06 - Education and the Enlightenment: The Conceptual Revolution

- Oct 29---Discussion

- Oct 30---Presentation: *Rights, Reason, and Revolution*

  





## WEEK 10: NOV 4–10
THE EIGHTEENTH CENTURY


- Readings:

  #. Boyd Ch 10 – The Eighteenth Century
  #. Bowen Vol3 Ch 06 - Education and the Enlightenment
  #. Bowen Vol3 Ch 07 - Theoretical Foundations of Education for the New Order
  #. Bowen Vol3 Ch 08 - Beginnings of National Systems

- Nov 5---Discussion

- Nov 6---Presentation: *Progress, Nature, Romance*

  
  
  
  
  

## WEEK 11: NOV 11–17
THE NINETEENTH CENTURY

- Readings:

  #. Boyd Ch 11 – The First Half of the Nineteenth Century
  #. Bowen Vol3 Ch 09 – The Bourgeois Epoch in Europe

- Nov 12---Discussion

- Nov 13---Presentation: *The Brave New World*





  

## WEEK 12: NOV 18–24
THE NINETEENTH CENTURY


- Readings:

  #. Boyd Ch 12 – The Second Half of the Nineteenth Century
  #. Bowen Vol3 Ch 10 - Science and Education
  #. Bowen Vol3 Ch 11 - Utopian and Progressive Movements in Europe
  

- Nov 19---Discussion

- Nov 20---Presentation: *Progressivism, Dada, Jazz*

  





## WEEK 13: NOV 25–DEC 1
THE TWENTIETH CENTURY

- Readings:

  #. Bowen Vol3 Ch 12 - Progressivism in the United States
  #. Bowen Vol3 Ch 13 - The Rise of National Planning

- Nov 26---Discussion

- Nov 27---Presentation: *Post-War "Miracles"*

  
  
  
  
  

## WEEK 14: DEC 2–8
THE TWENTIETH CENTURY

- Readings:

  #. Bowen Vol3 Ch 15 – Patterns of Development

- Dec 3---Discussion

- Dec 4---Presentation: *Posthumanism and the Anthropocene*





  

## WEEK 15: DEC 9–15
THE TWENTY-FIRST CENTURY

- Readings: To be announced

- Dec 10---Discussion

- Dec 11---Final Wrap-up






## WEEK 16: DEC 16–22
FINALS

- Dec 17---No Class

- Dec 18---No Class

**Final essay due by December 23rd at 00:00 (midnight)**



# About the Instructor


- PhD in Education, Yonsei University, 2022
- BA in Liberal Studies, University of Hawai'i, 2013



- Born and raised in Northern California.
- Became a naturalized citizen of Korea in 2023. 
- Father of three children, ages 8, 4, and 2.
