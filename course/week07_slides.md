---
title: The Scientific Revolution & The Enlightenment
subtitle: "Week IX: The Seventeenth Century"
date: 2023-10-17
---

::: notes

* chirascoro, tenebrism; relation to "enlightenment"
* east india company
  * coffee? influence on thought
	* coffehouses: "penny universities"
	  * anyone could and did attend; cost just a penny
		* social levelers; anyone could experience rich intellectual culture of the time
		* became center of public debate
		* promoted print books and news
		* helpt boost financial markets: insuarance, stocks, auctions etc.
		  * became meeting places for businesses and discussion of newspapers
			* some would post stock prices or had salesrooms attached---auction houses
		* criticized for intellectuals gravitating to them rather than studying
		* in 1800s coffehouses were established publicly as alternative to pubs!
    * in a way, more effective than schooling in the sense that they were more vital and organic.
      * they became whatever their patronage made them to be. through them, entire institutions of society were developed and transformed.
      * **demonstration of trend that education and learning was being explored outside of the school. people wanted more out of it, wanted it to be something different...not just studying of old books and ideas.**
      * this also demonstrates point that the reformations ideas for a new society needed to be "secular" to work...the direction must come from the common interests of the people, not the sanctioned opninions of the classics of classicists 
* salons
  * popular in france
	* hosted by an influential host in their home, usually a woman, and often in her bedroom
	* perhaps less "public" and "open" than coffeehouses
* emergence of public sphere in contrast to courtly society
  * see salons and coffeehouses above
	* point is that, even people involved in business and government would meet here
	* interactions were had between classes, and greater exposure to ideas
	* became almost clubs: different affliations would meet it preferred coffehouse
* the americas and asia
  * tobacco: massacre of jamestown
* quote from zinn, powhatan

:::


##

![Las Meninas, Velasquez](media/las_meninas.jpg){height=80%}

##

![The Four Continents, Rubens](media/four_contients_rubens.jpg){height=80%}

##

![Set design for Les Noches de Thetis, by Torelli](media/tetis_ballet_royal.jpg){height=80%}

##

![Ecstasy of St. Teresa, Bernini [(CC-BY-SA Livioandronico2013)](https://commons.wikimedia.org/wiki/File:Ecstasy_of_St._Teresa_HDR.jpg)](media/st_teresa.jpg){height=80%}

##

![Vanitas Stilleben, van Oosterwijck](media/vanitas_still_life.jpg){height=80%}

##

![The Night Watch, Rembrandt](media/night_watch_rembrandt.jpg){height=80%}

##

![Calling of St. Matthew, Caravaggio](media/st_matthew_caravaggo.jpg){height=80%}

##

![Cusco Cathedral [(CC-BY Martin St-Amant)](https://commons.wikimedia.org/wiki/File:Cath%C3%A9drale_de_Cusco_D%C3%A9cembre_2007e.jpg)](media/cusco_cathedral.jpg){height=80%}

##

![Hall of Mirrors, Versailles [(CC-BY-SA Myrabella)](https://commons.wikimedia.org/wiki/File:Chateau_Versailles_Galerie_des_Glaces.jpg)](media/hall_of_mirrors_versailles.jpg){height=80%}

##

![Porte Saint-Denis, Paris [(CC-BY-SA Wowo2008)](https://commons.wikimedia.org/wiki/File:Paris_-_Boulevard_Saint-Denis_Porte_Saint-Denis.jpg)](media/porte_saint-denis.jpg){height=80%}

##

![Garden at Palace of Versailles [(CC-BY-SA ToucanWings)](https://commons.wikimedia.org/wiki/File:Vue_a%C3%A9rienne_du_domaine_de_Versailles_le_20_ao%C3%BBt_2014_par_ToucanWings_-_Creative_Commons_By_Sa_3.0_-_22.jpg)](media/garden_versailles.jpg){height=80%}

##

![Lutheran Hymnal](media/lutheran_hymnal.jpg){height=80%}

::: notes

- traditionally church music was always monotonal, in some cases it was just chanting.
- in early dark ages, intsrumentation was also considered inappropriate
- during reformation, two views emerged:
  - hymns should only be songs using words directly quoted from the bible
  - others like luther saw hymns as a way to teach religious lessons
- mostly retained monotonal nature

:::

##

::: notes

- baroque music was not well-received at the time
- it was very experimental, which meant it had many successes and many failures too
- harder to appreciate. rousseau characterizes it as having confused melodies and "harsh" voices
- new styles, like the fugue
- new instruments, like the piano
- many experiments in virtuosity of the instrument:
  - significant, because at the time there was a lot of tension between the liberal and mechanical arts
  - also, appreciation of and ability in arts was part of court culture
    - lots of patronage, celebrity, etc.
- greater availability of printed music meant works could circulate (as well as ideas), and that composers could be famous

:::

![J. S. Bach](media/js_bach.jpg){height=80%}

##

![Antonio Vivaldi](media/vivaldi.jpg){height=80%}


##

![VOC Headquarters ca. 1670~90](media/voc_headquarters.jpg){height=80%}

##

![VOC Coinage [(CC-BY-SA Petrov Eduard)](https://commons.wikimedia.org/wiki/File:Duit_1735_-_Netherlands_East-Indies_(Dutch_East_India_Company._Holland).jpg)](media/voc_coin.jpg){height=80%}

##

![VOC outpost in Bengal](media/voc_bengal.jpg){height=80%}

##

![VOC outpost in Batavia](media/voc_batavia.jpg){height=80%}

##

![English Coffehouse](media/english_coffeehouse.jpg){height=80%}

##

![Turkish Coffeehouse](media/islamic_coffeehouse.jpg){height=80%}

##

![Women of a Salon, 1636](media/salon.jpg){height=80%}

##

![Decartes' *Meditations*](media/descartes_meditations.jpg){height=80%}

##

![Title page of Don Quixote](media/don_quixote.jpg){height=80%}

##

![First Logarithm Tables, 1617 Henry Briggs](media/log_tables.jpg){height=80%}

##

![Napier's bones [(CC-BY-SA Stephencdickson)](https://commons.wikimedia.org/wiki/File:An_18th_century_set_of_Napier%27s_Bones.JPG)](media/napiers_bones.jpg){height=80%}

##

![Pascal's Mechanical Calculators [(CC-BY-SA Edal Anton Lefterov)](https://commons.wikimedia.org/wiki/File:17th-century-mechanical-calculators_-Detail.jpg)](media/pascal_calculator.jpg){height=80%}

##

![Leibniz's Stepped Reckoner](media/stepped_reckoner.jpg){height=80%}

::: notes

"It is beneath the dignity of excellent men to waste their time in calculation when any peasant could do the work just as accurately with the aid of a machine."

:::

##

![Chief 'Powhatan' (Opechancanough)](media/powhatan.jpg){height=80%}

##

![Jamestown Massacre](media/jamestown_massacre.jpg){height=80%}

## Letter from Powhatan to John Smith

> I wish \[my brothers, sisters, and daughters\] to know as much as I do, and that your love to them may be like mine to you. *Why will you take by force what you can have quietly by love? Why will you destroy us who supply you with food? What can you get by war? We can hide our provisions and run into the woods; then you will starve for wronging your friends.* Why are you jealous of us? We are unarmed, and willing to give you what you ask, if you come in a friendly manner... In these wars, my men must sit up watching, and if a twig break, they all cry out "Here comes Captain Smith!" So I must end my miserable life. Take away your guns and swords, the cause of all our jealousy, or you may die in the same manner.

## English Colonists' Justification of Indigenous Slaughter

Psalms 2:8:


> Ask of me, and I shall give thee, the heathen for thine inheritance, and the uttermost parts of the earth for thy possession."

Romans 13:2


> Whosoever therefore resisteth the power, resisteth the ordinance of God: and they that resist shall receive to themselves damnation.

::: notes

- so what is going on here?

- native americans are the lost nations from babel
- they are ignorrant of gods word, so they needn't be saved, but...then why did they go through all the trouble to do so?
- many reasons:
  - one is that, they wanted what they had: resources. conquistadors would read latin bible phrases out loud just before attacking and even during battle. what is the real intention here?
  - natives always explained that europeans were jealous...were they? why could europeans not conceive of becoming wendatized so to speak, but the wendat could imagine being europeanized?

:::

##

![New France](media/new_france.jpg){height=80%}

##

![The Jesuit Relations](media/jesuit_relations.jpg){height=80%}

::: notes

- magazine used to raise money for jesuit mission
- gives an inside look into lives of native americans
- explains how they were totally free: no money, no power over others, no laws
- baffling to europeans: 
  - could not understand how people could live this way
  - even more baffled by how eloquent and critical the average native was
  - society was operated through free debate. it was inconceiveable to the French that they could debate so well without having ever read Cicero

:::

## Missionaries account of Montagnais-Naskapi people in 1642, *The Jesuit Relations*

> They imagine that they ought to by right of birth, to enjoy the liberty of wild ass colts, rendering no homage to any one whomsoever, except when they like. They have reproached me a hundred times because we fear our Captains, while they laugh at and make sport of theirs. All the authority of their chief is in his tongue's end; for **he is powerful in so far as he is eloquent; and, even if he kills himself talking and haranguing, he will not be obeyed unless he pleases the Savages**.

## Father Lallemant on the Wendat in 1644, *The Jesuit Relations*

> I do not believe that there is any people on earth freer than they, and less able to allow the subjection of their wills to any power whatever---so much so that Fathers here have no control over their children, Captains over their subjects, or the Laws of the country over any of them, except in so far as each is pleased to submit to them. There is no punishment which is inflicted on the guilty, and no criminal who is not sure that his life and property are in no danger.

## 

![Kandiaronk's Signature](media/kandiaronk.png){height=80%}


## Debate between Kandiaronk and Governor of Montreal

**Kandiaronk**:

> I have spent 6 years reflecting on the state of European society and I still can’t think of a single way they act that is not inhuman and I generally think this can only be the case as long as you stick to your distinctions of “mine” and “thine.” ... Do you seriously imagine that I would be happy to live like one of the inhabitants of Paris? To take two hours every morning just to put on my shirt and make up? To bow and scrape before every obnoxious galoot I meet on the street who happens to have been born with an inheritance? Do you actually imagine I could carry a purse full of coins and not immediately hand them over to people who are hungry? That I would carry a sword but not immediately draw it on the first band of thugs I see rounding up the destitute to press them into Naval service?

## Debate between Kandiaronk and Governor of Montreal (cont'd)

**Callière**:

>  Try, for once in your life to actually listen. Can't you see, my dear friend, that the nations of Europe could not survive without gold and silver or some similar precious symbol? Without it, nobles, priests, merchants and any number of others who lack the strength to work the soil would simply die of hunger. Our kings would not be kings. What soldiers would we have? Who would work for Kings or anyone else?

## Debate between Kandiaronk and Governor of Montreal (cont'd)

**Kandiaronk**:

> You honestly think you're going to sway me by appealing to the needs of nobles, merchants, and priests? If you abandoned conceptions of mine and thine, yes, such distinctions between men would dissolve. A leveling equality would take place among you, as it now does among the Wyandotte ... Over and over I have set forth the qualities that we Wyandotte believe ought to define humanity: wisdom, reason, equity, etc. and demonstrated that the existence of separate material interest knocks all these on the head. A man motivated by interest cannot be a man of reason.


