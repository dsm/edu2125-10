TEMPLATES=templates
SRC=course
BUILD=blog

REQS=$(SRC)/%.md

COMMON_OPTS = \
  --defaults=defaults \
  --defaults=references \
  --output $@ \
  $<

help:
	@echo ''
	@echo 'Build options for: "History of Western Education"'
	@echo ''
	@echo 'Usage:'
	@echo '   make help             show this message'
	@echo '   make protocol         build single protocol for $WEEK'
	@echo '   make slides           build slides for $WEEK'
	@echo '   make syllabus         build course syllabus'
	@echo '   make all              build all formats'

$(BUILD)/%protocol.md: $(SRC)/%protocol.md
	pandoc \
		--defaults=defaults_cmark \
    --lua-filter kill_h1.lua \
		$(COMMON_OPTS)

$(BUILD)/%protocol.pdf: $(SRC)/%protocol.md
	pandoc \
    --template=$(TEMPLATES)/protocol.tex \
    --metadata-file=protocol-tex.yml \
    --defaults=defaults_pdf \
    $(COMMON_OPTS)


$(BUILD)/%slides.pdf: $(SRC)/%slides.md
	pandoc \
    --template=$(TEMPLATES)/slides.tex \
    --defaults=defaults_pdf \
    --metadata-file=slides.yml \
    $(COMMON_OPTS)


$(BUILD)/%.pdf: $(SRC)/%.ty
	typst compile $< $@

$(SRC)/%.ty: $(REQS)
	pandoc \
    --template=$(TEMPLATES)/$(*F).ty \
    -t typst \
    --standalone \
    $(COMMON_OPTS)

$(BUILD)/%.md: $(REQS)
	pandoc \
		--defaults=defaults_cmark \
		$(COMMON_OPTS)


syllabus: $(SRC)/syllabus.md $(SRC)/수업계획서.md
	make $(BUILD)/syllabus.md
	make $(BUILD)/수업계획서.md
	rm -f $(SRC)/syllabus.ty
	rm -f $(SRC)/수업계획서.ty
	make $(SRC)/syllabus.ty
	make $(BUILD)/syllabus.pdf
	make $(SRC)/수업계획서.ty
	make $(BUILD)/수업계획서.pdf
  
protocols:
	for p in `ls $(SRC) | grep protocol.md` ; do \
    make "$(BUILD)/$$p";\
    make "$(BUILD)/`basename $$p .md`.pdf";\
	done

all: syllabus protocols
.PHONY: protocols syllabus slides help 
