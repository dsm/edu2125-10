#let (l-margin, r-margin) = (1.5in, 1.5in)

#let horizontalrule = [
  #line(start: (25%,0%), end: (75%,0%))
]

#let endnote(num, contents) = [
  #stack(dir: ltr, spacing: 3pt, super[#num], contents)
]

#show terms: it => {
  it.children
    .map(child => [
      #strong[#child.term]
      #block(inset: (left: 1.5em, top: -0.4em))[#child.description]
      ])
    .join()
}

#set table(
  inset: 6pt,
  stroke: none
)


#let yonsei-navy = rgb("#002065")

#let summary(body) = block(
  above: 2em, stroke: 0.5pt + yonsei-navy,
  width: 100%, inset: 14pt, radius: 3pt
)[
  #set text(fill: yonsei-navy)
  #place(
    top + left,
    dy: -6pt - 14pt, // Account for inset of block
    dx: 6pt - 14pt,
    block(fill: white, inset: 2pt)[*Introduction*]
  )
  #body
]

#let conf(
  title: none,
  subtitle: none,
  author: (),
  keywords: (),
  date: "",
  semester: "",
  course-info: (),
  abstract: none,
  cols: 1,
  margin: (left: l-margin, right: r-margin, y: 1.25in),
  paper: "a4",
  lang: "en",
  region: "US",
  font: ("IBM Plex Sans", "IBM Plex Sans KR", "IBM Plex Sans TC"),
  fontsize: 11pt,
  sectionnumbering: none,
  intro: [],
  doc,
) = {
  set document(
    title: title,
    author: (author.name),
    keywords: keywords,
  )
  set page(
    paper: paper,
    margin: margin,
    numbering: "1",
    header: locate(loc => {
  if counter(page).at(loc).first() > 1 [
    #text(weight: "light", size: .7em)[#semester]
    #h(1fr)
    #text(weight: "light", size: .7em)[#subtitle]
  ]
})
  )
  set par(justify: true)
  set text(lang: lang,
           region: region,
           font: font,
           size: fontsize)
  set heading(numbering: sectionnumbering)

  if title != none {
    align(center)[
      #text(weight: "bold", size: 2.5em, fill: yonsei-navy)[#title]
    ]
  }

    if subtitle != none {
    align(center)[
      #text(size: 1.6em)[#subtitle\
    #semester]]
  }

  if course-info != none {
    align(center)[
      #text(size: 1.1em)[#course-info.room ← #course-info.schedule\
      #course-info.college\
      #course-info.university, #course-info.city
    ]
    ]
  }

  if author != none {
    align(center)[
      #text(size: 1.25em)[#author.name\
      #author.email\
      \
      
    ]]
}

  if intro != none {

    summary[#intro]
}
  


  if cols == 1 {
    doc
  } else {
    columns(cols, doc)
  }
}


#let aside(content) = block(stroke: .25pt + yonsei-navy, inset: .5em, width: 100%, radius: 3pt)[#text(size: .8em, weight: "light", fill: yonsei-navy, content)]


#show heading.where(level: 1): it => {
  set align(left)
  set text(size: 1.4em, weight: "extrabold", fill: yonsei-navy)
  block(above: 2.2em, height: 0em, it.body)
  line(start: (100%,0%), end: (0%,0%), stroke: 2pt + yonsei-navy)
}

#show heading.where(level: 2): it => {
  set align(left)
  set text(size: 1.2em, weight: "extrabold", fill: yonsei-navy)
  block(above: 1.8em, it.body)
}


#show heading.where(level: 3): it => {
  set align(left)
  set text(size: 1em, weight: "extrabold", fill: yonsei-navy)
  block(above: 1.6em, it.body)
}


#show: doc => conf(
  title: [$course$],
  subtitle: [$coursenum$ 수업계획서],
  author:
    ( name: "꽃기린 (David Meyer), PhD",
      affiliation: "$institute$",
      email: "dsm@namu.blue" ),
    semester: "$semester$",
    course-info: (
      room: "$room$",
      schedule: "$sched$",
      college: "$college$",
      university: "$institute$",
      city: "$location$"
    ),
  font: ("IBM Plex Sans", "IBM Plex Sans KR", "IBM Plex Sans TC"),
  cols: 1,
  lang: "ko",
  region: "KR",
  intro: [$intro$],
  doc,
)


$body$
